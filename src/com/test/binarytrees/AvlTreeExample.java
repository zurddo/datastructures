package com.test.binarytrees;

public class AvlTreeExample {

    public static void main(String[] args) {
        AvlTree avlTree = new AvlTree();

        avlTree.insert(5);
        avlTree.insert(7);
        avlTree.insert(3);
        avlTree.insert(1);
        avlTree.insert(9);
        avlTree.insert(6);

        System.out.println(avlTree);
    }
}
