package com.test.binarytrees;

public class ExampleBinaryTree {

  private static SimpleBinaryTree simpleBinaryTree = new SimpleBinaryTree();

  public static void main(String[] args) {
    initTree();

    traversInOrder();
    testContaints();

    //testDelete("Delete...", 6);
    //testDelete("Delete no 2...", 4);

    traversPreOrder();
    traversPostOrder();
  }

  private static void traversPostOrder() {
    System.out.println("Travers tree post-order");
    simpleBinaryTree.traversPostOrder(simpleBinaryTree.getRoot());
    System.out.println("Post order no recursion");
    simpleBinaryTree.traversPostOrderNoRec(simpleBinaryTree.getRoot());
  }

  private static void traversPreOrder() {
    System.out.println("Travers tree pre-order");
    simpleBinaryTree.traversPreOrder(simpleBinaryTree.getRoot());
    System.out.println("no recursion: ");
    simpleBinaryTree.traversPreOrderNoRec(simpleBinaryTree.getRoot());
  }

  private static void testDelete(String s, int i) {
    System.out.println(s);
    simpleBinaryTree.delete(i);
    System.out.println("Elements after delete: " + simpleBinaryTree.getCount());
    simpleBinaryTree.traversInOrder(simpleBinaryTree.getRoot());
  }

  private static void testContaints() {
    System.out.println("Tree contains 3: " + simpleBinaryTree.containsValue(3));
    System.out.println("Tree conrains 10: " + simpleBinaryTree.containsValue(10));
  }

  private static void traversInOrder() {
    System.out.println("Travers tree in order");
    simpleBinaryTree.traversInOrder(simpleBinaryTree.getRoot());
    System.out.println("In order without recursion");
    simpleBinaryTree.traversInOrderNoRec(simpleBinaryTree.getRoot());
  }

  public static void initTree(){
    simpleBinaryTree.add(4);
    simpleBinaryTree.add(2);
    simpleBinaryTree.add(1);
    simpleBinaryTree.add(3);
    simpleBinaryTree.add(6);
    simpleBinaryTree.add(5);
    simpleBinaryTree.add(7);
    System.out.println("Number of elements in the tree: " + simpleBinaryTree.getCount());
  }
}
