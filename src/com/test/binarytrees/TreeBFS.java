package com.test.binarytrees;

import java.util.*;

public class TreeBFS<T> {

    private T value;
    private List<TreeBFS<T>> children;

    public TreeBFS(T value) {
        this.value = value;
        this.children = new ArrayList<>();
    }

    public TreeBFS<T> addChild(T value){
        TreeBFS<T> newChild = new TreeBFS<>(value);
        children.add(newChild);
        return newChild;
    }

    public static <T> Optional<TreeBFS<T>> search(T value, TreeBFS<T> root) {
        // Beginning, add root to the queue
        Queue<TreeBFS<T>> queue = new ArrayDeque<>();
        queue.add(root);

        while (!queue.isEmpty()){
            TreeBFS<T> temp = queue.remove(); // Dequeue element from the queue
            System.out.println("Visited node with value: " + temp.value);
            if(temp.value == value){
                return Optional.of(temp);
            }

            queue.addAll(temp.children);
        }
        return Optional.empty();
    }
}
