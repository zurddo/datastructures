package com.test.binarytrees;

public class BreadthFirstSearch {

    public static void main(String[] args) {

        System.out.println("Breadth first search using a Tree");
        TreeBFS<Integer> root = new TreeBFS(10);
        TreeBFS<Integer> rootFirstChild = root.addChild(2);
        TreeBFS<Integer> depthMostChild = rootFirstChild.addChild(3);
        TreeBFS<Integer> rootSecondChild = root.addChild(4);
        System.out.println(TreeBFS.search(4, root));

        System.out.println("Breadth first search using a Binary Tree");
        SimpleBinaryTree simpleBinaryTreeRoot = new SimpleBinaryTree();
        simpleBinaryTreeRoot.add(4);
        simpleBinaryTreeRoot.add(6);
        simpleBinaryTreeRoot.add(5);
        simpleBinaryTreeRoot.add(7);
        simpleBinaryTreeRoot.add(2);
        simpleBinaryTreeRoot.add(1);
        simpleBinaryTreeRoot.add(3);

        System.out.println(simpleBinaryTreeRoot.searchValueBFS(simpleBinaryTreeRoot.getRoot(), 5));

    }
}
