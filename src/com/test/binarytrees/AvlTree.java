package com.test.binarytrees;

public class AvlTree {

    private Node root;

    void updateHeight(Node node) {
        node.height = 1 + Math.max(height(node.left), height(node.right));
    }

    int height(Node node){
        return node == null ? -1 : node.height;
    }

    int getBalance(Node node) {
        return (node == null) ? 0 : height(node.right) - height(node.left); // returns 0, 1 or -1
    }

    public void insert(int value){
        root = insert(root, value);
    }

    public void delete(int value) {
        root = delete(root, value);
    }

    public Node getRoot() {
        return root;
    }

    public int getHeight(){
        return root.height;
    }

    // Insert a node in the tree
    Node insert(Node current, int value){
        if(current == null){
            return new Node(value);
        }
        // if value is < current.value
        if (value < current.value){
            current.left = insert(current.left, value);
        } else if (value > current.value){
            current.right = insert(current.right, value);
        } else {
            throw new RuntimeException("Duplicate key");
        }
        // For AVL we need to rebalance the tree after inserting a new value
        return rebalance(current);
    }

    // Delete a node in the tree
    Node delete(Node node, int value){
        if (node == null){
            return node;
        } else if(value < node.value){
            node.left = delete(node.left, value);
        } else if(value > node.value){
            node.right = delete(node.right, value);
        } else { // We find a value to delete
            // Check three cases 1. Node with only one children, 3. Node with both children
            //CASE 1:
            if(node.left == null || node.right == null){
                node = (node.left == null) ? node.right : node.left;
            } else { // has two nodes
                Node mostLeftChild = findMostLeftChild(node.right);
                node.value = mostLeftChild.value;
                node.right = delete(node.right, node.value); // To delete the leftmost child because we promoted it
            }
        }

        if (node != null){
            node = rebalance(node);
        }
        return node;
    }

    // Find value/node in the tree
    Node findNode(int value) {
        Node current = root;
        while (current != null) {
            if (current.value == value){
                break;
            }
            current = value < current.value ? current.left : current.right;
        }

        return current;
    }

    private Node findMostLeftChild(Node node) {
        return node.left == null ? node : findMostLeftChild(node.left);
    }

    private Node rebalance(Node current) {
        updateHeight(current); // Update height otherwise it will be always zero.
        int balanceFactor = getBalance(current);

        // Unbalance on the right side
        if (balanceFactor > 1){
            // Two cases
            // CASE 1: Height of the right right child is > than height of right left child
            if (height(current.right.right) > height(current.right.left)){
                // Rotate to left
                current = rotateLeft(current);
            } else { // CASE 2: Height of right right child is < height of right left child
                // Perform two rotations, first to right then to left
                current.right = rotateRight(current.right);
                current = rotateLeft(current);

            }
        } else if (balanceFactor < -1){ // unbalance on the left side
            // Two cases as well
            // CASE 1: Height of the left left child > height of left right child
            if(height(current.left.left) > height(current.left.right)){
                current = rotateRight(current);
            }else {
                current.left = rotateLeft(current.left);
                current = rotateRight(current);
            }
        }
        return current;
    }

    private Node rotateRight(Node current) {
        Node x = current.left;
        Node z = x.right;

        x.right = current;
        current.left = z;

        updateHeight(current);
        updateHeight(x);

        return x;
    }

    private Node rotateLeft(Node current) {
        Node x = current.right;
        Node z = x.left;

        x.left = current;
        current.right = z;

        updateHeight(current);
        updateHeight(x);

        // current no longer the root after the rotation
        return x;
    }


    class Node {
        int value;
        int height;
        Node left;
        Node right;

        public Node(int value) {
            this.value = value;
        }

    }
}
