package com.test.binarytrees;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;

public class SimpleBinaryTree {

  private Node root;
  private int count;

  // Adding data to the tree
  public void add(int value){
    root = addRecursive(root, value);
    count++;
  }

  // Recursive method to add values to binary tree
  private Node addRecursive(Node current, int value){
    if(current == null) {
      return new Node(value);
    }

    if(value < current.value){
      current.left = addRecursive(current.left, value);
    } else { // case > and equals to treat as larger and insert to the right
      current.right = addRecursive(current.right, value);
    }

    return current;

  }

  // Search value using Breadth first search
  public Node searchValueBFS(Node current, int value){
    Queue<Node> queue = new ArrayDeque<>();
    queue.add(current);

    while(!queue.isEmpty()) {
      Node temp = queue.remove();
      System.out.println("Binary tree visit: " + temp.value);
      if(value == temp.value){
        return current;
      }

      if (temp.left != null){
        queue.add(temp.left);
      }

      if (temp.right != null){
        queue.add(temp.right);
      }

    }

    return null;

  }

  // Check if tree contains element
  public boolean containsValue(int value){
    return containsValueRecursive(root, value);
  }

  /**
   * Search for an element in the tree and return true if the element exists Depth First Search
   * @param current node
   * @param value to serach
   * @return true if the element exists, false otherwise
   */
  private boolean containsValueRecursive(Node current, int value) {
    if(current == null) {
      return false;
    }

    if(current.value == value){
      return true;
    }

    if(value < current.value) {
      return containsValueRecursive(current.left, value);
    }

    return containsValueRecursive(current.right, value);
  }


  // Delete an element of the tree
  public void delete(int value) {
    root = deleteRecursive(root, value);
    count--;
  }

  /**
   * Method to delete recursively a value in a tree
   * @param current node
   * @param value to delete
   * @return the node
   */
  private Node deleteRecursive(Node current, int value){
    // Find element to delete as we did in contains method
    if(current == null) {
      return null;
    }

    if(current.value == value){
      // Delete element, consider 3 cases
      // Case 1: No children
      if(current.left == null && current.right == null){
        return null;
      }

      // Case 2: only 1 child
      if(current.right == null) {
        return current.left;
      }

      if(current.left == null) {
        return current.right;
      }

      // Case 3: 2 children
      int smallestValue = findSmallestValue(current.right);
      current.value = smallestValue;
      current.right = deleteRecursive(current.right, smallestValue);
      return current;
    }

    if(value < current.value){
      current.left = deleteRecursive(current.left, value);
      return current;
    }

    current.right = deleteRecursive(current.right, value);

    return current;

  }

  // TODO implement delete in no recursive

  // Traversals Using a stack
  public void traversPreOrderNoRec(Node root) {
    Stack<Node> stack = new Stack<>();

    Node current = root;
    stack.push(root);

    while (!stack.isEmpty()){
      current = stack.pop();
      visit(current.value);
      if(current.right != null){ // pushing right so will be at the bottom of the stack and visit first left
        stack.push(current.right);
      }

      if (current.left != null) {
        stack.push(current.left);
      }
    }
  }

  public void traversInOrderNoRec(Node root) {
    Stack<Node> stack = new Stack<>();
    Node current = root;
    stack.push(root);

    while(!stack.isEmpty()){
      while (current.left != null) { // Insert to the stack all the left elements
        current = current.left;
        stack.push(current);
      }

      current = stack.pop();
      visit(current.value);  // visit element

      if (current.right != null){ // push all elements of the right
        current = current.right;
        stack.push(current);
      }
    }
  }

  public void traversPostOrderNoRec(Node root) {
    Stack<Node> stack = new Stack<>();
    Node current = root;
    Node prev = root;
    stack.push(root);

    while(!stack.empty()){
      current = stack.peek();
      boolean hasChild = current.left != null || current.right != null;
      boolean isPrevLastChild = prev == current.right || (prev == current.left && current.right == null);

      if (!hasChild || isPrevLastChild){
        current = stack.pop();
        visit(current.value);
        prev = current;
      } else {
        if (current.right != null){
          stack.push(current.right);
        }
        if (current.left != null) {
          stack.push(current.left);
        }
      }


    }
  }

  private int findSmallestValue(Node root) {
    return root.left == null ? root.value : findSmallestValue(root.left);
  }

  /**
   * In-Order traversal with recursion, print in order the values of the tree
   * traversInOrder(left)
   * visit(value)
   * traverInOrder(right)
   *
   * @param current node
   */
  public void traversInOrder(Node current){
    if(current == null){
      return;
    }
    traversInOrder(current.left);
    visit(current.value);
    traversInOrder(current.right);
  }

  /**
   * Pre-order traversal with recursion, print pre order the values of the tree
   * @param current node
   */
  public void traversPreOrder(Node current) {
    if(current == null) {
      return;
    }
    visit(current.value);
    traversPreOrder(current.left);
    traversPreOrder(current.right);
  }

  /**
   * Post-order traversal with recursion, print post order the values of the tree
   * @param current node
   */
  public void traversPostOrder(Node current) {
    if (current == null) {
      return;
    }
    traversPostOrder(current.left);
    traversPostOrder(current.right);
    visit(current.value);
  }

  // Visit method, prints value
  public void visit(int value){
    System.out.println(value);
  }

  public Node getRoot() {
    return root;
  }

  public void setRoot(Node root) {
    this.root = root;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }

  class Node {
    private int value;
    private Node left;
    private Node right;

    public Node(int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public void setValue(int value) {
      this.value = value;
    }

    public Node getLeft() {
      return left;
    }

    public void setLeft(Node left) {
      this.left = left;
    }

    public Node getRight() {
      return right;
    }

    public void setRight(Node right) {
      this.right = right;
    }
  }
}
