package com.test.stringSearch;

public class NaiveStringSearch {

    public static void main(String[] args){

        String text = "This is a test text";
        String pattern = "text";

        int foundAt = searchString(text, pattern);

        System.out.println("Found at (or not): " + foundAt);
    }

    public static int searchString(String text, String pattern){
        int textSize = text.length();
        int patterSize = pattern.length();

        for (int i = 0; i <= (textSize - patterSize); i++){
            int j; // j will hold the index if the patter is found

            for (j = 0; j < patterSize; j++){
                if (text.charAt(j + i) != pattern.charAt(j)){
                    break;
                }
            }
            if (j == patterSize){
                return i; // returns the first occurrence. If we have to count here we keep track of the times we found the letter
            }
        }
        return -1;
    }
}
