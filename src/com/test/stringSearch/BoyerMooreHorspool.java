package com.test.stringSearch;

public class BoyerMooreHorspool {

    public static void main(String[] args){
        String text = "This is a test text";
        String pattern = "text";

        int foundAt = searchStringBMH(text, pattern);

        System.out.println("Found at (or not): " + foundAt);
    }

    private static int searchStringBMH(String text, String pattern) {
        char[] textArray = text.toCharArray();
        char[] patterArray = pattern.toCharArray();

        int shift[] = new int[256];

        for (int k = 0; k < 256; k++) {
            shift[k] = patterArray.length;
        }

        for (int k = 0; k < patterArray.length - 1; k++){
            shift[patterArray[k]] = patterArray.length - 1 - k;
        }

        int i = 0, j = 0;

        while ((i + patterArray.length) <= textArray.length) {
            j = patterArray.length - 1;

            while (textArray[i + j] == patterArray[j]) {
                j -= 1;
                if (j < 0)
                    return i;
            }

            i = i + shift[textArray[i + patterArray.length - 1]];
        }
        return -1;
    }
}
