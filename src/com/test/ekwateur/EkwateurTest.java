package com.test.ekwateur;

import java.io.File;

public class EkwateurTest {

    static String fileName = "universe-solution";

    public static void main(String[] args) {
        System.out.println("Find in this directory: /tmp/test/");
        System.out.println("the file: " + fileName);
        System.out.println("It is located in: ");
        System.out.println(locateUniverseFormula());
    }

    //Given the path of a directory, find the given file and return the String path to the file
    //could be nested into other directories /a/b/thisMyFile

    public static String locateUniverseFormula() throws RuntimeException{

        File file = new File("/tmp/test/");
        return search(file);

    }

    static String search(File file) throws RuntimeException {
        String absolutePath = null;
        if (!file.isDirectory()) {
            if (fileName.equals(file.getName().toLowerCase())) {
                return file.getAbsolutePath();
            }
        } else { // is Directory
            if (!file.canRead()) {
                throw new RuntimeException("Permission Denied");
            } else {
                for (File dirList : file.listFiles()) {
                    absolutePath = search(dirList);
                }
            }
        }
        return absolutePath;
    }

}
