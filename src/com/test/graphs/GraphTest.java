package com.test.graphs;

import java.util.*;

public class GraphTest {

    public static void main(String[] args) {
        Graph graph = new Graph();
        graph.addVertex("Bob");
        graph.addVertex("Alice");
        graph.addVertex("Mark");
        graph.addVertex("Rob");
        graph.addVertex("Maria");
        graph.addEdge("Bob", "Alice");
        graph.addEdge("Bob", "Rob");
        graph.addEdge("Alice", "Mark");
        graph.addEdge("Rob", "Mark");
        graph.addEdge("Alice", "Maria");
        graph.addEdge("Rob", "Maria");

        System.out.println(graph.printGraph());

        System.out.println("Print in depth traversal");
        depthFirstTraversal(graph, "Bob").forEach(System.out::println);

        System.out.println("Print in breadth traversal");
        breadthFirstTraversal(graph, "Bob").forEach(System.out::println);
    }

    // Depth-first travers
    public static Set<String> depthFirstTraversal(Graph graph, String root) {
        Set<String> visited = new LinkedHashSet<String>();
        Stack<String> stack = new Stack<String>();

        stack.push(root);
        while (!stack.isEmpty()){
            String vertex = stack.pop();
            if(!visited.contains(vertex)){
                visited.add(vertex);
                for(Graph.Vertex v : graph.getAdjVertices(vertex)){
                    stack.push(v.label);
                }
            }
        }
        return visited;
    }

    // Breadth first travesal
    public static Set<String> breadthFirstTraversal(Graph graph, String root){
        Set<String> visited = new LinkedHashSet<String>();
        Queue<String> queue = new LinkedList<String>();

        queue.add(root);
        visited.add(root); // adding first element to the visited ones

        while(!queue.isEmpty()) {
            String vertex = queue.poll();
            for (Graph.Vertex v : graph.getAdjVertices(vertex)){
                if(!visited.contains(v.label)){
                    visited.add(v.label);
                    queue.add(v.label);
                }
            }

        }
        return visited;
    }
}
