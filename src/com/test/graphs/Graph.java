package com.test.graphs;

import java.util.*;

public class Graph {

    Map<Vertex, List<Vertex>> adjVertices;

    public Graph() {
        this.adjVertices = new HashMap<Vertex, List<Vertex>>();
    }

    // Add a vertex
    public void addVertex(String label) {
        adjVertices.putIfAbsent(new Vertex(label), new ArrayList<>());
    }

    // Remove vertex
    public void removeVertex(String label) {
        Vertex vertex = new Vertex(label);
        // Remove all links with vertex
        adjVertices.values().stream().forEach(e -> e.remove(vertex));
        // Remove vertex itself
        adjVertices.remove(vertex);
    }

    // Add an edge, Vertex should exist previously
    public void addEdge(String label1, String label2) {
        Vertex v1 = new Vertex(label1);
        Vertex v2 = new Vertex(label2);
        adjVertices.get(v1).add(v2);
        adjVertices.get(v2).add(v1);
    }

    // Remove edge
    public void removeEdge(String label1, String label2){
        Vertex v1 = new Vertex(label1);
        Vertex v2 = new Vertex(label2);
        List<Vertex> vertexList1 = adjVertices.get(v1);
        List<Vertex> vertexList2 = adjVertices.get(v2);
        if(vertexList1 != null){
            vertexList1.remove(v1);
        }
        if(vertexList2 != null) {
            vertexList2.remove(v2);
        }
    }

    // Get adjacent vertices
    public List<Vertex> getAdjVertices(String label) {
        return adjVertices.get(new Vertex(label));
    }

    // print graph
    public String printGraph() {
        StringBuilder sb = new StringBuilder();
        for(Vertex v : adjVertices.keySet()) {
            sb.append(v + ": ");
            sb.append(adjVertices.get(v));
            sb.append(" ");
        }
        return sb.toString();
    }

    public class Vertex {
        String label;

        public Vertex(String label) {
            this.label = label;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Vertex other = (Vertex) o;
            if (!getOuterType().equals(other.getOuterType()))
                return false;
            if (label == null) {
                if (other.label != null)
                    return false;
            } else if (!label.equals(other.label))
                return false;
            return true;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + ((label == null) ? 0 : label.hashCode());
            return result;
        }

        @Override
        public String toString() {
            return label;
        }

        private Graph getOuterType() {
            return Graph.this;
        }
    }
}
