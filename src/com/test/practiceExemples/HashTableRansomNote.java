package com.test.practiceExemples;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/ctci-ransom-note/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps
 */
public class HashTableRansomNote {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String[] mn = scanner.nextLine().split(" ");

        int m = Integer.parseInt(mn[0]);

        int n = Integer.parseInt(mn[1]);

        String[] magazine = new String[m];

        String[] magazineItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < m; i++) {
            String magazineItem = magazineItems[i];
            magazine[i] = magazineItem;
        }

        String[] note = new String[n];

        String[] noteItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            String noteItem = noteItems[i];
            note[i] = noteItem;
        }

        checkMagazine(magazine, note);

        scanner.close();
    }

    // Complete the checkMagazine function below.
    static void checkMagazine(String[] magazine, String[] note) {
        Map<String, Integer> map = new HashMap<>();
        String result = "";
        for(int i = 0; i < magazine.length; i ++){
            if(map.containsKey(magazine[i])){
                int count = map.get(magazine[i]);
                map.put(magazine[i], count + 1);
            } else {
                map.put(magazine[i], 1);
            }
        }

        for (int i = 0; i < note.length; i++){
            if (map.containsKey(note[i])){
                int count = map.get(note[i]);
                if (count == 0){
                    result = "No";
                    break;
                }

                map.put(note[i], count - 1);

            } else {
                result = "No";
                break;
            }
        }
        System.out.println(result.isEmpty() ? "Yes" : result);
    }
}
