package com.test.practiceExemples;

import java.io.IOException;
import java.util.*;

/**
 * https://www.hackerrank.com/challenges/luck-balance/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms
 *
 *
 * Input: k which is the number of important contest that Lena can lose
 *        contests: a 2D array of integers where each i contains two integers that represent the luck
 *        balance and the importance of that contest.
 *
 *        k = 2 // can loose only two contests
 *                      0       1       2     3
 *        contest = [[5, 1], [1, 1], [4,0], [3,1]]
 *                    |
 *                  represents
 *                  the luck
 *                  she can spend
 *                  or save
 *        Lena can lose either contest 1, 2,
 *                                     1, 4,
 *                                     2, 4
 *
 *        Maximum sum of contests she can loose with the max value of luck
 *        and wining the contest with lowest luck.
 *
 *        5 - 1 => lose
 *        3 - 1 => lose
 *        1 - 1 => win
 *        4 - 0 => lose  // maybe we can lose all non important contests
 *
 *        k = 3 // can lose up to 3 important contests
 *        contest2 = [[5, 1], [2, 1], [1, 1], [8, 1], [10, 0], [5, 0]]
 *
 *        1. Sort elements by importance and luck value
 *              [8, 1] => lose
 *              [5, 1] => lose
 *              [2, 1] => lose
 *              [1, 1] ---- min of important contest
 *              [10, 0] => lose
 *              [5, 0] => lose
 *
 *        sum = 0;
 *        for
 *        if (isImportanContest(contest)) // check if isImportant flag is 1 or 0
 *             if (i < k)
 *                  sum += value
 *             else
 *                  sum -= value
 *        else
 *            sum += value
 *        total amount = 8 + 5 + 2 + 10 + 5 - 1 = 29
 *
 * Output; Print single integer denoting the max amount of luck Lena
 * can have after all contests.
 *
 *
 */
public class LuckBalance {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nk = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nk[0]);

        int k = Integer.parseInt(nk[1]);

        int[][] contests = new int[n][2];

        for (int i = 0; i < n; i++) {
            String[] contestsRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 2; j++) {
                int contestsItem = Integer.parseInt(contestsRowItems[j]);
                contests[i][j] = contestsItem;
            }
        }

        int result = luckBalance(k, contests);

        System.out.println(result);
        //bufferedWriter.write(String.valueOf(result));
        //bufferedWriter.newLine();

        // bufferedWriter.close();

        scanner.close();
    }

    private static int luckBalance(int k, int[][] contests) {
        List<Contest> contestsList = new ArrayList<>();

        if(contests.length == 0 || contests.length > 10000) return 0;

        for(int i = 0; i < contests.length; i++){ // O(n)
            boolean isImportant = contests[i][1] != 0;
            contestsList.add(new Contest(contests[i][0], isImportant));
        }

        contestsList.sort(Comparator.comparing(Contest::getImportance).thenComparing(Contest::getLuck).reversed()); //O(n log n)
        int sum = 0;

        for(int i = 0; i < contestsList.size(); i++){ // O(n)
            Contest currentContest = contestsList.get(i); // O(1)
            if(currentContest.importance){
                if(i < k) {
                    sum += currentContest.luck;
                } else {
                    sum -= currentContest.luck;
                }
            } else {
                sum += currentContest.luck;
            }

        }

        return sum;

    }

    static class Contest {
        int luck;
        Boolean importance;

        public Contest(int luck, boolean importance){
            this.luck = luck;
            this.importance = importance;
        }

        public int getLuck() {
            return luck;
        }

        public void setLuck(int luck) {
            this.luck = luck;
        }

        public Boolean getImportance() {
            return importance;
        }

        public void setImportance(Boolean importance) {
            this.importance = importance;
        }

        //        @Override
//        public int compareTo(Contest contest) {
//
//            int importanceComp = contest.importance.compareTo(this.importance);
//
//            if( importanceComp != 0){
//                return importanceComp;
//            }
//
//            return this.luck - contest.luck;
//        }
    }
}
