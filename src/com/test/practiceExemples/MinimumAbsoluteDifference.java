package com.test.practiceExemples;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/minimum-absolute-difference-in-an-array/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms
 */
public class MinimumAbsoluteDifference {


    private static final Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        int result = minimumAbsoluteDifference(arr);

        System.out.println(result);

        //bufferedWriter.write(String.valueOf(result));
        //bufferedWriter.newLine();

        //bufferedWriter.close();

        scanner.close();
    }

    private static int minimumAbsoluteDifference(int[] arr) {
        // 1. Minimum difference we need to sort the array. The difference between two consecutive numbers
        // will give the minimum difference
        Arrays.sort(arr);
        int min= 0;
        for (int i = 0; i < arr.length; i++){
            int abs = Math.abs(arr[i] - arr[i +1]);
            if (abs < min) {
                min = abs;
            }
        }
        return min;
    }
}
