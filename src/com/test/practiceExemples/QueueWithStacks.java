package com.test.practiceExemples;

import java.util.Scanner;
import java.util.Stack;

public class QueueWithStacks {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main (String[] args) {

        Queue queue = new Queue();
        int queries = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < queries; i++){
            String[] query = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
            int command = Integer.parseInt(query[0]);
            if(command == 1){
                queue.enqueue(Integer.parseInt(query[1]));
            } else if (command == 2) {
                queue.dequeue();
            } else if (command == 3) {
                System.out.println(queue.peek());
            }
        }

    }

    static class Queue{

        Stack<Integer> stack1;
        Stack<Integer> stack2;
        int front;

        public Queue(){
            stack1 = new Stack<>();
            stack2 = new Stack<>();
        }

        public void enqueue(int element){
            if (stack1.isEmpty()){
                front = element;
            }
            stack1.push(element);
        }

        public void dequeue() {
            if(stack2.isEmpty()){
                // Fill stack2 with element from stack 1
                while (!stack1.isEmpty()){
                    stack2.push(stack1.pop());
                }
            }
            stack2.pop();
        }

        // Element next to be dequeue
        public int peek(){
            if (!stack2.isEmpty()){
                return stack2.peek();
            }
            return front;
        }
    }
}
