package com.test.practiceExemples;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/icecream-parlor/problem
 */
public class IceCreamParlor {

    // Complete the icecreamParlor function below.
    static int[] icecreamParlor(int m, int[] arr) {

        Map<Integer, Integer> map = new HashMap<>();
        int[] result = new int[2];
        // Skip validations for array based on constrains for simplicity
        for (int i = 0; i < arr.length; i++) {
            int y = m - arr[i];
            if (map.containsKey(y)){
                result[0] = map.get(y) + 1;
                result[1] = i + 1;
                break;
            }
            map.put(arr[i], i);
        }
        return result;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            int m = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            int n = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            int[] arr = new int[n];

            String[] arrItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int i = 0; i < n; i++) {
                int arrItem = Integer.parseInt(arrItems[i]);
                arr[i] = arrItem;
            }

            int[] result = icecreamParlor(m, arr);
            String resultString = "";
            for (int i = 0; i < result.length; i++) {
                //bufferedWriter.write(String.valueOf(result[i]));
                resultString.concat(String.valueOf(result[i]));
                if (i != result.length - 1) {
                    //bufferedWriter.write(" ");
                    resultString.concat(" ");
                }
            }
            System.out.println(resultString);
            //bufferedWriter.newLine();
        }

        //bufferedWriter.close();

        scanner.close();
    }
}
