package com.test.practiceExemples;

import java.util.Arrays;

public class MarkAndToys {

    // Complete the maximumToys function below.
    static int maximumToys(int[] prices, int k) {
        int totalOfToys = 0;
        Arrays.sort(prices);

        for (int i = 0; i < prices.length; i++){
            if(k >= prices[i]){
                k = k - prices[i];
                totalOfToys++;
            }
        }
        return totalOfToys;

    }

    public static void main(String[] args){
        int[] prices = {1, 12, 5, 111, 200, 1000, 10};
        int amount = 50;

        System.out.println(maximumToys(prices, amount));
    }
}
