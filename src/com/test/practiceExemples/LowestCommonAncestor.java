package com.test.practiceExemples;

import java.util.*;

/**
 * https://www.hackerrank.com/challenges/binary-search-tree-lowest-common-ancestor/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=trees
 *
 * Find the lowest common ancestor
 *
 * 1 <= n, node.data <= 25
 * 1 <= v1, v2 <= 25
 * v1 != v2
 *
 */
public class LowestCommonAncestor {

    /*
    class Node
    	int data;
    	Node left;
    	Node right;
	*/
    public static Node lca(Node root, int v1, int v2) {
        // Write your code here.
        if(root == null) return null;
        if(v1 == v2) return null;
        if(v1 > 25 || v2 > 25) return null;

        if(root.data > v1 && root.data > v2){
            return lca(root.left, v1, v2);
        }

        if (root.data < v1 && root.data < v2){
            return lca(root.right, v1, v2);
        }

        return root;

    }



    public static Node insert(Node root, int data) {
        if(root == null) {
            return new Node(data);
        } else {
            Node cur;
            if(data <= root.data) {
                cur = insert(root.left, data);
                root.left = cur;
            } else {
                cur = insert(root.right, data);
                root.right = cur;
            }
            return root;
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int t = scan.nextInt();
        Node root = null;
        while(t-- > 0) {
            int data = scan.nextInt();
            root = insert(root, data);
        }
        int v1 = scan.nextInt();
        int v2 = scan.nextInt();
        scan.close();
        Node ans = lca(root,v1,v2);
        System.out.println(ans.data);
    }

    static class Node {
        Node left;
        Node right;
        int data;

        Node(int data) {
            this.data = data;
            left = null;
            right = null;
        }
    }
}
