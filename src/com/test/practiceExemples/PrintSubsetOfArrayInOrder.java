package com.test.practiceExemples;

public class PrintSubsetOfArrayInOrder {

    public static void main(String[] args){
        int[] values = {1, 2, 3};

        printSubsets(values);
    }

    private static void printSubsets(int[] values) {
        // Need an array to keep track of what should we print
        int[] temp = new int[values.length];
        int index = 0; // starts at position 0

        findSubsets(values, temp, index);
    }

    private static void findSubsets(int[] values, int[] temp, int index) {
        // if index reaches the end then print values using temp to check which values should print
        if (index == temp.length - 1){
            temp[index] = 0;
            printValues(values, temp);
            temp[index] = 1;
            printValues(values, temp);
            return;
        }

        temp[index] = 0;
        findSubsets(values, temp, index + 1);
        temp[index] = 1;
        findSubsets(values, temp, index + 1);
    }

    private static void printValues(int[] values, int[] temp) {
        String result = "";
        boolean isNull = true;
        result += "{";
        // Iterate the values array and check if we have to print the value using temp array
        for (int i = 0; i < values.length; i++){
            if (temp[i] == 1){
                result += values[i] + "";
                isNull = false;
            }
        }

        if (!isNull) {
            result += "} ";
        }

        if (isNull){
            result += "Empty} ";
        }

        System.out.print(result);

    }
}
