package com.test.practiceExemples;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/ctci-array-left-rotation/problem
 */
public class RotateLeft {

    // Complete the rotLeft function below.
    // NOT OPTIMIZED for larger arrays => O(n^2)
    static int[] rotLeftNotOptimized(int[] a, int d) {
        // Iterate d times
        for (int i = 0; i < d; i++){
            int first = a[0];
            for (int j = 0; j < a.length; j++){
                if (j != a.length - 1){
                    a[j] = a[j + 1];
                    continue;
                }
                a[j] = first;
            }
        }

        return a;
    }

    static int[] rotLeft(int[] a, int d) {
        // new array to store values
        int arrayLength = a.length;
        int[] newArray = new int[arrayLength];
        // iterate d times
        for (int i = 0; i < arrayLength; i++){
            int newIndex = (i + d) % arrayLength;
            newArray[i] = a[newIndex];
        }
        return newArray;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        String[] nd = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nd[0]);

        int d = Integer.parseInt(nd[1]);

        int[] a = new int[n];

        String[] aItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int aItem = Integer.parseInt(aItems[i]);
            a[i] = aItem;
        }

        int[] result = rotLeft(a, d);

        System.out.println("Result is: " + Arrays.toString(result));

        scanner.close();
    }

}
