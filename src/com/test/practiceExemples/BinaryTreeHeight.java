package com.test.practiceExemples;
import java.util.*;
import java.io.*;

public class BinaryTreeHeight {

    /*
    class Node
        int data;
        Node left;
        Node right;
    */
    public static int height(Node root) {
        // Write your code here.
        int leftHeight = 0;
        int rightHeight = 0;
        int max = 0;

        if (root == null || isLeaf(root)) {
            return 0;
        }

        if(root.left != null) {
            leftHeight = height(root.left);
        }

        if (root.right != null) {
            rightHeight = height(root.right);
        }

        // Compare height of left and right and store the max of the two
        max = (leftHeight > rightHeight) ? leftHeight : rightHeight;

        return max + 1;
    }

    static boolean isLeaf(Node node){
        return node.left == null && node.right == null;
    }

    public static Node insert(Node root, int data) {
        if(root == null) {
            return new Node(data);
        } else {
            Node cur;
            if(data <= root.data) {
                cur = insert(root.left, data);
                root.left = cur;
            } else {
                cur = insert(root.right, data);
                root.right = cur;
            }
            return root;
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int t = scan.nextInt();
        Node root = null;
        while(t-- > 0) {
            int data = scan.nextInt();
            root = insert(root, data);
        }
        scan.close();
        int height = height(root);
        System.out.println(height);
    }

    static class Node {
        Node left;
        Node right;
        int data;

        Node(int data) {
            this.data = data;
            left = null;
            right = null;
        }
    }
}

