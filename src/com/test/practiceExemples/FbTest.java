package com.test.practiceExemples;

import java.util.*;

/**
 * Time exclusive for each program.
 *
 *  Name    Operation        Time
 *  A       start              0
 *  B       start              2
 *  B       end                4
 *  A       end                6
 *
 *
 *  Name    Operation        Time
 *   A       start              0
 *   B       start              2
 *   C       start              3
 *   C       end                4
 *   B       end                5
 *   A       end                6
 *
 * Assuming that programs come in order and ther is always start and end
 *
 */
public class FbTest {

    public static void main(String[] args){
        List<Program> programs = new ArrayList<>();
        programs.add(new Program("A", 0, 0));
        programs.add(new Program("B", 0, 2));
        programs.add(new Program("C", 0, 3));
        programs.add(new Program("C", 1, 4));
        programs.add(new Program("B", 1, 5));
        programs.add(new Program("A", 1, 6));

        List<Program> result = calculateTimeExclusive(programs);

        for (Program program : result) {
            System.out.println(program.name + ": " + program.timeExclusive);
        }
    }

    public static List<Program> calculateTimeExclusive(List<Program> programs) {
        Stack<Program> stack = new Stack<>();
        List<Program> result = new ArrayList<>();  // Using a list for simplicity could be an array.
        int diffTime = 0;

        if(programs.isEmpty()) {
            return null;
        }

        for(Program program: programs){  // O(n)
            // check if the element is start or end
            if(program.operationType == 0){
                stack.push(program); // O(1)
            } else {
                if (stack.isEmpty()) {
                    return null; // Exception or something else in case can't calculate
                }
                Program last = stack.peek();
                if (last.name.equals(program.name)) {
                    int exclusive = program.time - last.time - diffTime; // calculate the time exclusive
                    diffTime = program.time - last.time; // update the diff for next element if any.
                    result.add(new Program(program.name, exclusive));
                    stack.pop();
                } else {
                    return null; // Exception or something else in case don't match
                }
            }
        }
        return result;
    }

    public static class Program {
        private String name;
        private int operationType; // it will be 0 for start and 1 for end
        private int time;
        private int timeExclusive;

        public Program(String name, int timeExclusive){
            this.name = name;
            this.timeExclusive = timeExclusive;
        }

        public Program(String name, int operationType, int time) {
            this.name = name;
            this.operationType = operationType;
            this.time = time;
        }
    }

}
