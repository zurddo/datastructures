package com.test.practiceExemples;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

/**
 * Problem and test data in:
 * https://www.hackerrank.com/challenges/swap-nodes-algo/problem
 * Swap nodes in a tree, represented as a bi dimensional array
 *
 */
public class SwapNodes {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = Integer.parseInt(scanner.nextLine().trim());

        int[][] indexes = new int[n][2];

        for (int indexesRowItr = 0; indexesRowItr < n; indexesRowItr++) {
            String[] indexesRowItems = scanner.nextLine().split(" ");

            for (int indexesColumnItr = 0; indexesColumnItr < 2; indexesColumnItr++) {
                int indexesItem = Integer.parseInt(indexesRowItems[indexesColumnItr].trim());
                indexes[indexesRowItr][indexesColumnItr] = indexesItem;
            }
        }

        int queriesCount = Integer.parseInt(scanner.nextLine().trim());

        int[] queries = new int[queriesCount];

        for (int queriesItr = 0; queriesItr < queriesCount; queriesItr++) {
            int queriesItem = Integer.parseInt(scanner.nextLine().trim());
            queries[queriesItr] = queriesItem;
        }

        int[][] result = swapNodes(indexes, queries);



        /*for (int resultRowItr = 0; resultRowItr < result.length; resultRowItr++) {
            for (int resultColumnItr = 0; resultColumnItr < result[resultRowItr].length; resultColumnItr++) {
                //bufferedWriter.write(String.valueOf(result[resultRowItr][resultColumnItr]));

                if (resultColumnItr != result[resultRowItr].length - 1) {
                    //bufferedWriter.write(" ");
                }
            }

            if (resultRowItr != result.length - 1) {
                //bufferedWriter.write("\n");
            }
        }*/

        //bufferedWriter.newLine();

        //bufferedWriter.close();
    }

    static int[][] swapNodes(int[][] indexes, int[] queries) {
        /*
         * Write your code here.
         */
        // Array or arrays result
        int[][] result = new int[queries.length][];
        // 1. Create tree
        Node root = createTree(indexes);
        // 2. Iterate queries
        for (int i = 0; i < queries.length; i++){
            // Iterate BFS
            result[i] = searchAndSwap(root, queries[i]);
        }

        return result;
    }

    private static int[] searchAndSwap(Node root, int k){
        // Get height of tree => how many levels does it has?
        int[] result = new int[root.size];
        int level = getHeight(root);

        // Iterate through the levels and check if level is a multiple of k
        for (int i = 1; i <= level; i++){
            if (i % k == 0){
                // swap children of nodes at this level
                swapAtLevel(root, i);
                // get into an array the print in order transversal of the tree
                getInOrderTraversal(root, result);
            }
        }

        return result;
    }

    private static void getInOrderTraversal(Node root, int[] result) {
        int index = 0;
        Stack<Node> stack = new Stack<>();
        Node current = root;
        //stack.push(root);

        while (current != null || !stack.isEmpty()){
            while(current != null){
                stack.push(current);
                current = current.left;
            }

            current = stack.pop();
            result[index] = current.value;

            current = current.right;
            index++;
        }

    }

    private static void swapAtLevel(Node current, int level){
        if (current == null) {
            return;
        }

        if (level == 1) {
            Node temp = current.left;
            current.left = current.right;
            current.right = temp;
        } else if (level > 1) {
            swapAtLevel(current.left, level - 1);
            swapAtLevel(current.right, level - 1);
        }
    }

    public static int getHeight(Node current){
        if (current == null) {
            return 0;
        }

        int leftHeight = getHeight(current.left);
        int rightHeight = getHeight(current.right);

        if (leftHeight > rightHeight) {
            return leftHeight + 1;
        } else {
            return rightHeight + 1;
        }
    }

    public static Node createTree(int[][] indexes){
        Node root = createTree(indexes, new Node(1), 1);
        root.size = getSizeOfTree(root);
        return root;
    }

    private static Node createTree(int[][] indexes, Node root, int data) {

        if (data == -1) {
            return null;
        }

        if(root == null){
            root = new Node(data);
        }

        root.left = createTree(indexes, root.left, indexes[data - 1][0]);
        root.right = createTree(indexes, root.right, indexes[data - 1][1]);

        return root;
    }

    private static int getSizeOfTree(Node root){
        int count = 0;
        Queue<Node> queue = new ArrayDeque<>();
        queue.add(root);

        while (!queue.isEmpty()){
            Node temp = queue.remove();

            if(temp.left != null) {
                queue.add(temp.left);
            }

            if (temp.right != null) {
                queue.add(temp.right);
            }

            count++;

        }

        return count;
    }

    static class Node {
        int value;
        int size;
        Node left;
        Node right;
        public Node(int value) {
            this.value = value;
        }
    }

}
