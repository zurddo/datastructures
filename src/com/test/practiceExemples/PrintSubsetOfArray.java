package com.test.practiceExemples;

/**
 * Prints all the subsets of an array
 * https://algorithms.tutorialhorizon.com/print-all-sub-sequences-of-a-given-array/
 *
 * int [] a = {1, 2, 3};
 * Output: Possible sub sequences –
 * {Empty}, {1}, {2}, {3}, {1, 2} ,{1,3}, {2, 3}, {1, 2, 3}
 */
public class PrintSubsetOfArray {

    public static void main(String[] args){
        int[] values = {1, 2, 3};

        printSubsets(values);
    }

    private static void printSubsets(int[] values) {
        // Need an array to keep track of what should we print
        int[] temp = new int[values.length];
        int index = 0; // starts at position 0

        findSubsets(values, temp, index);
    }

    private static void findSubsets(int[] values, int[] temp, int index) {
        // if index reaches the end then print values using temp to check which values should print
        if (index == values.length){
            printValues(values, temp);
            return;
        }

        temp[index] = 0;
        findSubsets(values, temp, index + 1);
        temp[index] = 1;
        findSubsets(values, temp, index + 1);
    }

    private static void printValues(int[] values, int[] temp) {
        String result = "";

        // Iterate the temp array and check if we have to print the value
        for (int i = 0; i < temp.length; i++){
            if (temp[i] == 1){
                result += values[i] + ",";
            }
        }

        if (result.isEmpty()){
            result = "{Empty string}";
        }

        System.out.println(result);

    }
}
