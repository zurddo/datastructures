package com.test.practiceExemples;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class LargestRectangle {

    // Complete the largestRectangle function below.
    static long largestRectangle(int[] h) {

        Stack<int[]> s = new Stack<>(); // Create stack of span = [x0, x1]
        int n = h.length;
        h = Arrays.copyOf(h, n+1); // Append a sentinel to array h
        int x1;
        int maximum = 0;
        for(int x0 = 0; x0 <= n; x0++) {
            for(x1 = x0; !s.isEmpty() && h[s.peek()[0]] >= h[x0]; ) {
                int[] x = s.pop();
                x1 = x[1];
                maximum = Math.max(maximum, h[x[0]] * (x0 - x1));
            }
            s.push(new int[]{x0, x1});
        }
        return maximum;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] h = new int[n];

        String[] hItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int hItem = Integer.parseInt(hItems[i]);
            h[i] = hItem;
        }

        long result = largestRectangle(h);

        System.out.println(result);
        //bufferedWriter.write(String.valueOf(result));
        //bufferedWriter.newLine();

        //bufferedWriter.close();

        scanner.close();
    }
}
