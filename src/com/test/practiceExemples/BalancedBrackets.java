package com.test.practiceExemples;

import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;

/**
 * https://www.hackerrank.com/challenges/balanced-brackets/problem
 *
 */
public class BalancedBrackets {

    // Complete the isBalanced function below.
    static String isBalanced(String s) {
        Stack<Character> stack = new Stack();
        if (s.isEmpty()){
            return "YES";
        }

        for (char c : s.toCharArray()){
            if (c == '[' || c == '{' || c == '('){
                stack.push(c);
            }

            if (c == ']' || c == '}' || c == ')'){
                if (stack.isEmpty()){
                    return "NO";
                }
                // If stack not empty, peek in the stack
                char last = stack.peek();
                if (c == '}' && last == '{' ||
                    c == ']' && last == '[' ||
                    c == ')' && last == '(') {
                    stack.pop();
                } else {
                    return "NO";
                }
            }
        }
        return stack.isEmpty() ? "YES" : "NO";
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            String s = scanner.nextLine();

            String result = isBalanced(s);

            System.out.println(result);

        }
        scanner.close();
    }

}
