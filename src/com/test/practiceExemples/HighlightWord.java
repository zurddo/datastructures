package com.test.practiceExemples;

import java.io.IOException;
import java.util.Scanner;

public class HighlightWord {

    // Complete the designerPdfViewer function below.
    static int designerPdfViewer(int[] h, String word) {
        int maxHeight = 1;

        if(word.length() > 10) {
            throw new IllegalArgumentException("the word is bigger than 10 letters long");
        }

        for (char c : word.toCharArray()){
            int charValue = ((int) c - 97);
            if (h[charValue] > 7 || h[charValue] < 1){
                throw new IllegalArgumentException("The high of the letter is out of limits");
            }
            if (h[charValue] > maxHeight) {
                maxHeight = h[charValue];
            }
        }

        return word.length() * maxHeight;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int[] h = new int[26];

        String[] hItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < 26; i++) {
            int hItem = Integer.parseInt(hItems[i]);
            h[i] = hItem;
        }

        String word = scanner.nextLine();

        int result = designerPdfViewer(h, word);

        System.out.println(result);

        scanner.close();
    }
}
