package com.test.practiceExemples;

import java.io.IOException;
import java.util.*;

/**
 * https://www.hackerrank.com/challenges/fraudulent-activity-notifications/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting
 *
 * This passes the base cases but for a big amount of data it will fail.
 *
 * Need to improve implementing "Counting sort" TODO: investigate about it
 *
 */
public class HackerLandBank {

    // Complete the activityNotifications function below.
    static int activityNotifications(int[] expenditure, int d) {

        if(expenditure.length > 200000){
            return 0;
        }

        if (d > expenditure.length){
            return 0;
        }

        int expLength = expenditure.length;
        int notifications = 0;
        for(int i = d; i < expLength; i++){
            int[] temp = new int[d];
            float med = 0.0f;
            boolean isEven = d % 2 == 0;
            for(int j = i - d, k = 0; j <= i - 1; j++, k++) {
                temp[k] = expenditure[j];
                if(isEven) {
                    med += expenditure[j] / (d * 1.0);
                }
            }
            Arrays.sort(temp);
            if(!isEven){
                // This is odd number, take the median value
                int medValue = (d - 1) / 2;
                med = temp[medValue];
            }

            // Current expense is >= median
            if(expenditure[i] >= med * 2){
                notifications++;
            }
        }
        return notifications;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nd = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nd[0]);

        int d = Integer.parseInt(nd[1]);

        int[] expenditure = new int[n];

        String[] expenditureItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int expenditureItem = Integer.parseInt(expenditureItems[i]);
            expenditure[i] = expenditureItem;
        }

        int result = activityNotifications(expenditure, d);

        System.out.println(result);
      /*  bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();*/

        scanner.close();
    }
}
