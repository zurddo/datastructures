package com.test.practiceExemples;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/new-year-chaos/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=arrays
 */
public class NeyYearChaos {

    // Complete the minimumBribes function below.
    static void minimumBribes(int[] q) {
        int totalBribes = 0;
        // Iterate through the array and check for swaps
        for(int i = q.length - 1; i >= 0; i--){
            //int realPos = Math.abs(2 - (q[i] + 1));
            if(q[i] - (i + 1) > 2){
                System.out.println("Too chaotic");
                break;
            }

            int max = Math.max(0, q[i] - 2);
            for (int j = max; j < i; j++){
                if (q[j] > q[i]){
                    totalBribes++;
                }
            }

//            if(realPos == i){
//                continue;
//            }
//            if(realPos - i > 2){
//                System.out.println("Too chaotic");
//            } else {
//                totalBribes += (realPos - i);
//                //swap(q, i, i + 1);
//            }
        }
        System.out.println(totalBribes);
    }

    static void swap(int[] q, int indexOne, int indexTwo){
        int temp = q[indexOne];
        q[indexOne] = q[indexTwo];
        q[indexTwo] = temp;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            int n = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            int[] q = new int[n];

            String[] qItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int i = 0; i < n; i++) {
                int qItem = Integer.parseInt(qItems[i]);
                q[i] = qItem;
            }

            minimumBribes(q);
        }

        scanner.close();
    }
}
