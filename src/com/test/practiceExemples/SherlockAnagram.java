package com.test.practiceExemples;

import java.io.IOException;
import java.util.*;

public class SherlockAnagram {

    // Complete the sherlockAndAnagrams function below.
    static int sherlockAndAnagrams(String s) {
        int count = 0;
        s = s.trim();
        if(s.length() < 2) {
            return 0;
        }

        s = s.toLowerCase();

        Map<String, Integer> map = new HashMap<>();

        for (int i = 0; i < s.length(); i++){
            for (int j = i + 1; j <= s.length(); j++){
                //int[] frequencyMap = new int[26];
                String substring = s.substring(i, j);
                // Sort substring (array of char) and add to set/map
                // if another substring sorted is in the map then its an anagram.
                char[] subStringChar = substring.toCharArray();
                Arrays.sort(subStringChar);
                substring = String.valueOf(subStringChar);

                if(map.containsKey(substring)){
                    int value = map.get(substring);
                    count = count + value;
                    map.put(substring, value + 1); // maybe not needed
                }else {
                    map.put(substring, 1);
                }
//                for (int k = 0; k < substring.length(); k++){
//                    frequencyMap[substring.charAt(k) - 'a']++;
//                }

//                if(map.containsKey(frequencyMap)){
//                    int value = map.getOrDefault(frequencyMap, 0);
//                    map.put(frequencyMap, value + 1);
//                }
//                map.put(frequencyMap, 1);
            }
        }

        return count;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int q = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int qItr = 0; qItr < q; qItr++) {
            String s = scanner.nextLine();

            int result = sherlockAndAnagrams(s);

            System.out.println(result);
          //  bufferedWriter.write(String.valueOf(result));
            //bufferedWriter.newLine();
        }

        //bufferedWriter.close();

        scanner.close();
    }
}
