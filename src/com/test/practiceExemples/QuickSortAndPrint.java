package com.test.practiceExemples;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/quicksort2/problem
 *
 * This quick sort takes the first element as pivot
 */
public class QuickSortAndPrint {

    public static void main(String[] args){
        int[] input = {5, 8, 1, 3, 7, 9, 2};

        System.out.println("Before sorting: " + Arrays.toString(input));

        quickSort(input);

        System.out.println("After sorting: " + Arrays.toString(input));
    }

    static void quickSort(int[] array){
        elem_div(array, 0, array.length - 1);
    }

    private static void quickSort(int[] array, int start, int end) {
       if (start < end) {
           int partitionIndex = partition(array, start, end);

           quickSort(array, start, partitionIndex - 1);
           quickSort(array, partitionIndex, end);
           printArray(array,start,end);

       }
    }

    private static void printArray(int[] array, int start, int end) {
        for(int n = start; n <= end; n++){
            System.out.print(array[n]+" ");
        }
        System.out.println("");
    }

    private static int partition(int[] input, int start, int end) {

        int pivotValue = input[start];

        while(start <= end){ // moving left towards the center
            while (input[start] < pivotValue){ // try to find an element that should be in the right side
                start++;
            }

            while (input[end] > pivotValue){ // try to find an element that should be on the left side
                end--;
            }

            if (start <= end){
                swap(input, start, end);
                start++;
                end--;
            }

        }

        return start;

    }

    private static void swap(int[] input, int start, int end) {
        int temp = input[start];
        input[start] = input[end];
        input[end] = temp;
    }

    // From responses
    static void elem_div(int[] ar,int start, int end){

        if(start<end)
        {
            int comp_elem=ar[start];
            int j;
            int i;

            for(i=start;i<=end;i++)
            {
                /* Shifting elemnts small than the compare element move left */
                /* Right Elements get automatically adjusted, no need to check them */
                if(ar[i]<comp_elem)
                {

                    int small_elem=ar[i];
                    for(j=i-1;j>=start;j--)
                    {
                        if(ar[j]>=comp_elem)
                        {
                            ar[j+1]=ar[j];
                        }
                        else
                        {
                            ar[j+1]=small_elem;
                            break;
                        }
                    }

                    /* In case the element found is the smallest element of the list */
                    if(j<start)
                    {
                        ar[start]=small_elem;
                    }
                }
            }

            for(i=start;i<=end;i++)
            {
                if(ar[i]==comp_elem)
                    break;
            }
            elem_div(ar,start,i-1);
            elem_div(ar,i+1,end);
            printArray(ar,start,end);
        }
        return;
    }



}
