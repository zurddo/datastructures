package com.test.practiceExemples;

public class MultiplyStrings {

    public static void main(String[] args){
        String num1 = "498828660196";
        String num2 = "840477629533";
        System.out.println(multiply(num1, num2));
    }

    private static String multiply(String num1, String num2) {

        int[] resultArray = new int[num1.length() + num2.length()];

        for (int i = num1.length() - 1, j = 0; i > 0; i--, j++){
            for (int k = num2.length() - 1, l = 0; k > 0; k--, l++){
                resultArray[j + l] += (num1.charAt(i) - '0') * (num2.charAt(k) - '0');
            }
        }

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < resultArray.length; i++){
            int mod = resultArray[i] % 10;
            int carry = resultArray[i] / 10;
            if (i + 1 < resultArray.length){
                resultArray[i + 1] += carry;
            }
            sb.insert(0, mod);
        }

        while (sb.charAt(0) == '0' && sb.length() > 1){
            sb.deleteCharAt(0);
        }

        return sb.toString();

    }
}
