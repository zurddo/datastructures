package com.test.practiceExemples;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/crush/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays
 *
 *  Input: length of an array, 1-indexed
 *          bidimensional array of queries which will contain the bounds left and right, and the k
 *          number to sum up.
 *
 *          n = 10 // length of array
 *          queries = [[1, 5, 3], [4, 8, 7], [6 ,9 ,1]]
 *                      |              |            |
 *                    left bound    right bound    value to sum up
 *
 *          array of 10 elements start at 0
 *          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
 *           1  2  3  4  5  6  7  8  9  10
 *           0  1  2  3  4  5  6  7  8  9 <= real values internal array
 *
 *          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
 *          [3, 0, 0, 7, -3, 1, 0, -7, -1, 0]
 *
 *           3
 *           10
 *           Iterate through queries then set bounds, and iterate through array bounds inclusive
 *           and to each element add the k value
 *
 *           return the max value in the array
 *
 *
 */
public class ArrayManipulation {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nm = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nm[0]);

        int m = Integer.parseInt(nm[1]);

        int[][] queries = new int[m][3];

        for (int i = 0; i < m; i++) {
            String[] queriesRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 3; j++) {
                int queriesItem = Integer.parseInt(queriesRowItems[j]);
                queries[i][j] = queriesItem;
            }
        }

        long result = arrayManipulation(n, queries);

        System.out.println(result);
        //bufferedWriter.write(String.valueOf(result));
        //bufferedWriter.newLine();

        //bufferedWriter.close();

        scanner.close();
    }

    // O(n * m) => O(n log n) => O(m * n log n)
    /**
     * That's what I understand this theory. We can try to understand this logic like we imagine
     * Supermario walking on a N width horiz line. a and b is the point on the line, k is the mushroom Mario
     * like to eat. When Mario go to the point at a,he eat the k size mushroom and become taller,after he have
     * walked through point b, his height reverse to the origin height before he eat the mushroom.
     * eg.
     * 1. When Mario is walking to a, he eat a k size mushroom, and become k bigger
     * 2. Then Mario is walking to a', he eat a k' size mush, and become k' bigger, now Mario's height
     *    is (k + k')
     * 3. If Mario have walked to b, so he pooped out the mushroom and become k smaller, the only way that
     *    he can become larger is to meet a new (a,b) point and eat a new k size mushroom
     * 4. The rest can be done in the same manner.
     *
     *    What we need to do is tracing the Mario's biggest height when walking through that muliple query's a and b point.
     */
    private static long arrayManipulation(int n, int[][] queries) {
        long[] mainArray = new long[n];

        if(queries.length > 200000){
            return 0;
        }

        if (n < 3){
            return 0;
        }

        for(int i = 0; i < queries.length; i++){ // O(n)
            int leftBound = queries[i][0];
            int rightBound = queries[i][1];
            int value = queries[i][2];

            if(leftBound > rightBound){
                continue;
            }

            mainArray[leftBound - 1] += value;
            if (rightBound < n){
                mainArray[rightBound] -= value;
            }

//            for(int j = leftBound - 1; j <= rightBound - 1; j++){ // O(m)
//                mainArray[j] += value;
//            }
        }

        long temp = 0;
        long max = 0;

        for (int i = 0; i < n; i++){
            temp += mainArray[i];
            if (temp > max) {
                max = temp;
            }
        }

        ///Arrays.sort(mainArray); // O(nlogn)

        return max;
    }
}
