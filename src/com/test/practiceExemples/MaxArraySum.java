package com.test.practiceExemples;

import java.io.IOException;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/max-array-sum/problem
 */
public class MaxArraySum {

    // Complete the maxSubsetSum function below.
    // Using kandane's algorithm (variation)
    static int maxSubsetSum(int[] arr) {

        int length = arr.length;
        int max = arr[0]; // first element as the max at the beginning
        int current = 0;
        int next;

        for (int i = 1; i < length; i++){
            next = Math.max(current, max);
            max = current + arr[i];
            current = next;
        }

        return Math.max(current, max);

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        int res = maxSubsetSum(arr);

        System.out.println(res);
        //bufferedWriter.write(String.valueOf(res));
        //bufferedWriter.newLine();

        //bufferedWriter.close();

        scanner.close();
    }
}
