package com.test.practiceExemples;

public class SumNumbersInLinkedList {

    public static void main(String[] args){
        /*ListNode listNode3 = new ListNode(3);
        ListNode listNode2 = new ListNode(4, listNode3);
        ListNode listNode = new ListNode(2, listNode2);

        ListNode listNode4 = new ListNode(4);
        ListNode listNode6 = new ListNode(6, listNode4);*/
        ListNode listNode2 = new ListNode(8);
        ListNode listNode = new ListNode(1, listNode2);
        ListNode listNode5 = new ListNode(0);

        ListNode result = addTwoNumbers(listNode, listNode5);
        System.out.println("Result; " + result);

    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {

        int carry = 0;

        ListNode result = addTwoNumbers(l1, l2, carry);

        return result;

    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2, int carry) {

        int sum = 0;
        ListNode l1Next, l2Next;

        if(l1 == null && l2 == null){
            if (carry != 0){
                return new ListNode(carry);
            }
            return null;
        }

        if(l1 == null || l2 == null) {
            if (l1 == null) {
                sum = l2.val + carry;
                l1Next = null;
                l2Next = l2.next;
               /* if(sum >= 10){
                    sum = sum % 10;
                    carry = 1;
                }else {
                    carry = 0;
                }*/
                //return new ListNode(sum, addTwoNumbers(null, l2.next, carry));
            } else {
                sum = l1.val + carry;
                l1Next = l1.next;
                l2Next = null;
                /*if(sum >= 10){
                    sum = sum % 10;
                    carry = 1;
                }else {
                    carry = 0;
                }*/
                //return new ListNode(sum, addTwoNumbers(l1.next, null, carry));
            }
        } else {
            sum = l1.val + l2.val + carry;
            l1Next = l1.next;
            l2Next = l2.next;
        }
        // sum = l1.val + l2.val + carry;
        if(sum >= 10){
            sum = sum % 10;
            carry = 1;
        }else {
            carry = 0;
        }

        return new ListNode(sum, addTwoNumbers(l1Next, l2Next, carry));
    }

    public static class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }
}
