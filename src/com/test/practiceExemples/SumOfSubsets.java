package com.test.practiceExemples;

import java.util.HashMap;
import java.util.Map;

public class SumOfSubsets {

    public static void main(String[] args) {
        int[] input = {2, 4, 6, 10};
        int total = 21;

        int result = countSubsets(input, total);
        System.out.println("Recursion, Number of subsets that total is: " + result);

        int resultCache = countSubsetsCache(input, total);
        System.out.println("Cache, memoized, Number of subsets that total is 10: " + resultCache);
    }

    private static int countSubsetsCache(int[] input, int total){
        Map<String, Integer> cache = new HashMap<>();
        return countSubsetsSumMem(input, total, input.length - 1, cache);

    }

    private static int countSubsets(int[] input, int total) {
        return countSubsetsSum(input, total, input.length - 1); // Go backwards
    }

    // Recursive method (not so efficient)
    private static int countSubsetsSum(int[] input, int total, int index) {
        if (total == 0) {
            return 1;
        }
        if (index < 0) { // outside boundaries
            return 0;
        }
        if (total < 0) { // there is no combination left to sum up the total
            return 0;
        }

        if(total < input[index]) {
            return countSubsetsSum(input, total, index - 1);
        } else {
            return countSubsetsSum(input, total - input[index], index - 1) // calculate if there is a possible number if total < current value in array
                    + countSubsetsSum(input, total, index - 1); // count the current value in the array.
        }
    }

    /**
     * Find how many subsets add up to the given total. using cache
     * Need a cache with a key, value pair because we need which total and index we alredy have
     * @param input
     * @param total
     * @param index
     * @param cache
     * @return
     */
    private static int countSubsetsSumMem(int[] input, int total, int index, Map<String, Integer> cache){
        // Create the key
        String key = total + ":" + index;
        if (cache.containsKey(key)){
            return cache.get(key);
        }
        if (total == 0) {
            return 1;
        }
        if (index < 0) { // outside boundaries
            return 0;
        }
        if (total < 0) { // there is no combination left to sum up the total
            return 0;
        }

        int count;
        if(total < input[index]) {
            count = countSubsetsSum(input, total, index - 1);
        } else {
            count = countSubsetsSum(input, total - input[index], index - 1) // calculate if there is a possible number if total < current value in array
                    + countSubsetsSum(input, total, index - 1); // count the current value in the array.
        }

        cache.put(key, count);
        return count;

    }
}
