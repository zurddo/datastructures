package com.test.practiceExemples;

import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/abbr/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dynamic-programming
 *
 * Input: two strings => a, b
 * a can have Capital letters and small letters.
 * b can only have Capital letters. ?
 *
 * Output: Print YES if it's possible to make string a equal to string b
 * otherwise, print NO.
 *
 *  We have to transform A and check if we can have the same B string
 *  a can capitalize more than 1 lowercase letters and drop all the remaining letters
 *
 *  1. Capitalize zero or more of a's lowercase letters.
 *  2. Delete all of the remaining lowercase letters in a.
 *
 *  a = AbcDE and b = ABDE
 *
 *  Check if all letter from B are in A => First idea is to iterate through b and check  for each
 *  letter of A if that exist in the string a (first capitalize string a).
 *
 *  Another solution could be making a map of the letter in an array, then capitalize all letters
 *  in a and create a map of a as well then compare the two maps and check if the have at least the
 *  same occurrences in the letters, if yes then print YES otherwise NO.
 *
 *  Third possible solution is to have a map with the letters of B and a counter,
 *  then check each letter of 'a' and check if letter at position i it's equal to their
 *  capital letter in the ascii value, the difference should be 32 always.
 *
 *  Should check if current letter is uppercase or lowercase because if doesn't contains
 *  and it's uppercase can't be dropped.
 *
 *  If map contains letter on 'a' continue to next letter and maybe counter decreases
 *  if doesn't contains then don't decrease counter but continue to next letter.
 *  in the loop before check if the map contains the letter we can check if counter is
 *  different of 0 which means that we still have letters.
 *
 *
 *
 */
public class Abbreviation {

    // Complete the abbreviation function below.
    static String abbreviation(String a, String b) { // overall O(n + m)
        boolean[][] isValid = new boolean[a.length()+1][b.length()+1];


        // array initialization - if a is non-empty but b is empty,
        // then isValid == true iff remaining(a) != contain uppercase
        isValid[0][0] = true;

        boolean containsUppercase = false;
        for (int k = 1; k <= a.length(); k++) {  // Starting at position 1
            int i = k - 1;
            // if the pointer at string a is uppercase
            if (a.charAt(i) >= 65 && a.charAt(i) <= 90 || containsUppercase) {
                containsUppercase = true;
                isValid[k][0] = false;
            }
            else isValid[k][0] = true;
        }
        // tabulation from start of string
        for (int k = 1; k <= a.length(); k++) {
            for (int l = 1; l <= b.length(); l++) {
                int i = k - 1; int j = l - 1; // Return to zero-based index

                // when the characters are equal, set = previous character bool.
                if (a.charAt(i) == b.charAt(j)) {
                    isValid[k][l] = isValid[k-1][l-1];
                }
                // if uppercase a == b, set = prev character bool. or just eat a.
                else if ((int) a.charAt(i) - 32 == (int) b.charAt(j)) {
                    isValid[k][l] = isValid[k-1][l-1] || isValid[k-1][l];
                }
                // if a is uppercase and no more b, or uppercase a is not b, then false
                else if (a.charAt(i) <= 90 && a.charAt(i) >= 65) {
                    isValid[k][l] = false;
                }
                //else just eat a
                else {
                    isValid[k][l] = isValid[k-1][l];
                }
            }
        }
        return isValid[a.length()][b.length()]? "YES" : "NO";

    }

    private static boolean isCapitalLetter(char letter) {
        return letter >= 65 && letter <= 90;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int q = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int qItr = 0; qItr < q; qItr++) {
            String a = scanner.nextLine();

            String b = scanner.nextLine();

            String result = abbreviation(a, b);

            System.out.println(result);
            //bufferedWriter.write(result);
            //bufferedWriter.newLine();
        }

        //bufferedWriter.close();

        scanner.close();
    }
}
