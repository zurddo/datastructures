package com.test.rakuten;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * To run the tests you need to add the Junit4 and hamcrest 2.2 dependencies to the classpath
 */
public class WordHashTest {

    @Test
    public void whenPutSingleValueAndGet_thenReturnsEmptyList() {
        WordHash wordHash = new WordHash();

        wordHash.add("sea");

        List<String> resultList = wordHash.get("sea");

        assertTrue(resultList.isEmpty());
    }

    @Test
    public void whenPutValuesAndGet_thenReturnsEmptyList() {
        WordHash wordHash = new WordHash();

        wordHash.add("ray");
        wordHash.add("ill");
        wordHash.add("toy");

        List<String> resultList = wordHash.get("queue");

        assertTrue(resultList.isEmpty());
    }

    @Test(expected = NullPointerException.class)
    public void whenGetNull_thenNullPointerException() {
        WordHash wordHash = new WordHash();

        wordHash.add("ray");
        wordHash.add("ill");
        wordHash.add("toy");

        List<String> resultList = wordHash.get(null);

    }

    @Test
    public void whenPutMultipleValues_thenReturnsListIfContainsValue() {
        WordHash wordHash = new WordHash();

        wordHash.add("sea");
        wordHash.add("fog");
        wordHash.add("duo");

        List<String> resultList = wordHash.get("fog");

        assertFalse(resultList.isEmpty());
        assertThat(resultList, hasSize(1));
        assertThat(resultList, contains("duo"));
    }

    @Test
    public void whenPutMultipleValues_thenReturnsListInOrder() {
        WordHash wordHash = new WordHash();

        wordHash.add("sea");
        wordHash.add("fog");
        wordHash.add("duo");
        wordHash.add("ted");
        wordHash.add("ray");
        wordHash.add("wig");
        wordHash.add("ill");
        wordHash.add("toy");
        wordHash.add("try");

        List<String> resultList = wordHash.get("cow");

        assertFalse(resultList.isEmpty());
        assertThat(resultList, hasSize(4));
        assertThat(resultList, contains("duo", "fog", "toy", "wig"));
    }

    @Test
    public void whenDuplicateValues_thenOneValueForDuplicate() {
        WordHash wordHash = new WordHash();

        wordHash.add("sea");
        wordHash.add("fog");
        wordHash.add("duo");
        wordHash.add("duo");

        List<String> resultList = wordHash.get("fog");

        assertFalse(resultList.isEmpty());
        assertThat(resultList, hasSize(1));
        assertThat(resultList, contains("duo"));
    }


    @Test(expected = NullPointerException.class)
    public void whenNullWord_thenNullPointerException() {
        WordHash wordHash = new WordHash();
        wordHash.add(null);
    }

    @Test
    public void whenRemove_thenCheckItsRemoved() {

        WordHash wordHash = new WordHash();
        wordHash.add("duo");
        wordHash.add("fog");

        boolean result = wordHash.remove("fog");

        assertThat(result, is(true));

    }

    @Test
    public void whenDontRemove_thenCheckIfRemoved() {

        WordHash wordHash = new WordHash();
        wordHash.add("duo");
        wordHash.add("fog");

        boolean result = wordHash.remove("cow");

        assertThat(result, is(false));

    }

    @Test
    public void whenRemove_thenCheckValueInHash() {

        WordHash wordHash = new WordHash();
        wordHash.add("duo");
        wordHash.add("fog");
        wordHash.add("wig");
        wordHash.add("ill");
        wordHash.add("toy");

        boolean result = wordHash.remove("toy");
        List<String> resultList = wordHash.get("cow");

        assertThat(result, is(true));
        assertFalse(resultList.isEmpty());
        assertThat(resultList, hasSize(3));
        assertThat(resultList, contains("duo", "fog", "wig"));

    }


}
