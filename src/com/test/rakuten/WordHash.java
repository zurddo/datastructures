package com.test.rakuten;

import java.util.*;

public class WordHash implements WordHashInterface{

    // Using hashMap that allows use null as values. Search in a Hashmap is O(1), worst case O(n)
    private HashMap<Word, Integer> wordHashMap = new HashMap<Word, Integer>();

    @Override
    public List<String> add(String word) {

        if(word == null)
            throw new NullPointerException();

        Word newWord = new Word(word);
        if (!wordHashMap.containsKey(newWord)) {
            wordHashMap.put(newWord, null);
        }

        return searchInMap(word);
    }

    // Uses same search to return when add an item.
    @Override
    public List<String> get(String word) {
        if(word == null)
            throw new NullPointerException();

        return searchInMap(word);
    }

    @Override
    public boolean remove(String word) {
        return wordHashMap.remove(new Word(word), null);
    }

    // Search through the map per char in given string.
    // Filter out the given string.
    // Using stream to apply filters and add the result to final list.
    // Maybe could use a frequency map?, TO CHECK if there is time.
    private List<String> searchInMap(String word) {
        List<String> resultList = new ArrayList<>();
        for(Character c : word.toCharArray()){
            wordHashMap.keySet()
                    .stream()
                    .filter(keyMap -> !keyMap.name.equals(word) &&
                            keyMap.name.indexOf(c) > 0)
                    .sorted(Comparator.comparing(o -> o.name))
                    .forEach( item -> {
                            if(!resultList.contains(item.name))
                                resultList.add(item.name);
                    });
        }

        return resultList;
    }

    class Word{
        String name;

        public Word(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        /**
         *
         * Override equals to get the same value with the same instance as key.
         * Use with hashCode() as well when get() from hashtable class.
         *
         * We assume 'name' is non-null otherwise check nullability
         */
        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;

            Word word = (Word) o;
            return getName().equals(word.getName());
        }

        /**
         * Get the hashcode of 'name' so it will be always the same.
         */
        @Override
        public int hashCode() {
            return getName().hashCode();
        }
    }
}
