package com.test.rakuten;

import java.util.List;

public interface WordHashInterface {

    // Add the word if it doesn't already exist
    // if exists returns a list of words where each contains a character from the
    // given word
    List<String> add(String word);

    // returns a list of words where each contains a character from given word
    List<String> get(String word);

    // remove the word
    // if exists return true, else return false
    boolean remove(String word);

}
