package com.test.linkedLists;

public class LinkedListMain {

  public static void main(String[] args){

    LinkedListExample linkedListExample = new LinkedListExample();

    System.out.println(linkedListExample.getSize());

    linkedListExample.add("Hello");
    linkedListExample.add("World");
    linkedListExample.add("!");

    for (Node n = linkedListExample.getHead(); n != null; n = n.getNextNode()){
      System.out.println(n.getValue());
    }

    System.out.println(linkedListExample.getSize());
    System.out.println(linkedListExample.getTail().getValue());
    linkedListExample.remove("World");
    System.out.println(linkedListExample.getSize());

    // Print all elements of a list
    for (Node n = linkedListExample.getHead(); n != null; n = n.getNextNode()){
      System.out.println(n.getValue());
    }

  }
}
