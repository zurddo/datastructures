package com.test.doublylinkedlist;

public class Node {

  private String value;
  private Node nextNode;
  private Node previousNode;

  public Node(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Node getNextNode() {
    return nextNode;
  }

  public void setNextNode(Node nextNode) {
    this.nextNode = nextNode;
  }

  public Node getPreviousNode() {
    return previousNode;
  }

  public void setPreviousNode(Node previousNode) {
    this.previousNode = previousNode;
  }
}
