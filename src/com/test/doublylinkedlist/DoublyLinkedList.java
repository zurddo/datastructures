package com.test.doublylinkedlist;

public class DoublyLinkedList {

  private Node head;
  private Node tail;
  private int size;

  public Node getHead() {
    return head;
  }

  private void setHead(Node head) {
    this.head = head;
  }

  public Node getTail() {
    return tail;
  }

  private void setTail(Node tail) {
    this.tail = tail;
  }

  public int getSize() {
    return size;
  }

  // Add the specified value to the start of the linked list
  public void addFirst(String value){
    addFirst(new Node(value));
  }

  // Add the specified Node to the start of the linked list
  public void addFirst(Node newNode){
    // Save the head to not lose it
    Node tmp = head;

    // Point head to the new node
    head = newNode;

    // Add the rest of the list to the head
    head.setNextNode(tmp);

    size++;

    if (size == 1){
      tail = head;
    } else {
      // Add double link to first
      tmp.setPreviousNode(head);
    }

  }

  // Adding a value to the tail of the list
  public void addLast(String value){
    addLast(new Node(value));
  }

  // Adding a node to the tail of the list
  public void addLast(Node newNode){

    if(size == 0){
      head = newNode;
    } else {
      tail.setNextNode(newNode);

      // double link to last
      newNode.setPreviousNode(tail);

    }

    tail = newNode;
    size++;
  }

  // Removing a node from the head
  public void removeFirst(){
    if(size != 0){
      head = head.getNextNode();
      size--;
      if (size == 0) {
        tail = null;
      } else {
        head.setPreviousNode(null );
      }
    }
  }

  // Removing a node from the tail
  public void removeLast(){
    if(size != 0) {
      if(size == 1){
        head = null;
        tail = null;
      } else {
        // doubly linked list
        // finds second to last node and change next to null and make it the tail
        tail.getNextNode().setNextNode(null);
        tail = tail.getPreviousNode();
      }
      size--;
    }
  }

  public void add(String value){
    addFirst(value);
  }

  // Implementation of a contains method
  public boolean contains(String value) {
    Node current = head;

    while(current != null) {
      if(current.getValue().equals(value)){
        return true;
      }
      current = current.getNextNode();
    }
    return false;
  }

  // Remove an item from the list by value
  public boolean remove(String value){
    Node current = head;
    Node previous = null;

    // 1. Empty list -> do nothing
    // 2. Single node ->
    // 3. Many nodes
    //    a. remove first node
    //    b. remove node in the middle

    while(current != null) {

      if(current.getValue().equals(value)){
        if(previous != null){
          // case 3b
          previous.setNextNode(current.getNextNode());

          if(current.getNextNode() == null){
            tail = previous;
          } else {
            current.getNextNode().setPreviousNode(previous);
          }
          size--;
        } else {
          // case 2 and 3a
          removeFirst();
        }
        return true;
      }

      previous = current;
      current = current.getNextNode();

    }

    return false;

  }

  // clear the list
  public void clear() {
    head = null;
    tail = null;
    size = 0;
  }
}
