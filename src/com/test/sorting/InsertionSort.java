package com.test.sorting;

import java.util.Arrays;

public class InsertionSort {

    public static void main(String[] args){

        int[] arrayInput = {10, 8, 99, 7, 1, 5, 88, 9};

        System.out.println("Before Sorting: " + Arrays.toString(arrayInput));

        insertionSort(arrayInput);

        System.out.println("After sorting ascending: " + Arrays.toString(arrayInput));
    }

    private static void insertionSort(int[] arrayInput) {
        int length = arrayInput.length;
        for (int i = 1; i < length; i++){ // i -> 1, 2, 3, 4
            int key = arrayInput[i]; // 8, 99, 7, 1
            int j = i - 1; // 0, 1, 2, 3

            // j = 2, 1, 0
            while (j >= 0 && arrayInput[j] > key){ // 10 > 8 - 10 > 99 - 99 > 7 - 10 > 7 - 8 > 7
                arrayInput[j + 1] = arrayInput[j]; // [8, 10, 99, 7] -> [8, 10, 99, 99] -> [8, 10, 10, 99] -> [8, 8, 10, 99]
                j--; // -1
            }
            arrayInput[j + 1] = key; // 1, 7, 8, 10, 99

            for (int k = 0; k <= length; k++){
                if (k == length){
                    System.out.println();
                } else {
                    System.out.print(arrayInput[k] + " ");
                }
            }
        }
    }
}
