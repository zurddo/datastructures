package com.test.sorting;

import java.util.Arrays;

public class MergeSort {

    public static void main(String[] args){

        int[] input = {10, 8, 99, 7, 1, 5, 88, 9};

        System.out.println("Before sorting: " + Arrays.toString(input));

        mergeSort(input, input.length);

        System.out.println("After sorting: " + Arrays.toString(input));

    }

    // This method is recursive, so we need the base and the recursive conditions
    private static void mergeSort(int[] input, int length) {
        if(length == 1){ // array in the unit
            return;
        }

        int mid = length / 2;
        int[] left = new int[mid];
        int[] right = new int[length - mid];

        // populate left array
        for (int i = 0; i < mid; i++){
            left[i] = input[i];
        }

        // populate right array
        for (int i = mid; i < length; i++){
            right[i - mid] = input[i];
        }

        // Recursive method
        mergeSort(left, mid);
        mergeSort(right, length - mid);

        // Join the arrays
        merge(input, left, right, mid, length - mid);
    }

    private static void merge(int[] input, int[] left, int[] right, int leftLength, int rightLength) {
        // Initial index of left and right arrays
        int i = 0, j = 0;
        int k = 0;

        while (i < leftLength && j < rightLength){
            if(left[i] <= right[j]){
                input[k++] = left[i++];
            } else {
                input[k++] = right[j++];
            }
        }

        // Add remaining from left
        while (i < leftLength){
            input[k++] = left[i++];
        }

        // Add remaining from right
        while (j < rightLength) {
            input[k++] = right[j++];
        }
    }
}
