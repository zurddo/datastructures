package com.test.sorting;

import java.util.Arrays;

public class Bubble {

    public static void main(String[] args) {

        int[] arrayInput2 = {7,3,4,4,6,8,5};
        int[] arrayInput = {10, 8, 99, 7, 1, 5, 88, 9};

        System.out.println("Before Sorting: " + Arrays.toString(arrayInput));

        bubbleSort(arrayInput);

        System.out.println("After sorting ascending: " + Arrays.toString(arrayInput));

        bubbleSort(arrayInput, false);

        System.out.println("After sorting descending: " + Arrays.toString(arrayInput));
    }

    // First try of bubble sorting
    private static void bubbleSortFirst(int[] arrayInput) {
        int temp;
        boolean stillSwaps = true;
        int swaps;
        while(stillSwaps) {
            swaps = 0;
            for (int i = 0; i < arrayInput.length - 1; i++) {
                if (arrayInput[i + 1] < arrayInput[i]) {
                    temp = arrayInput[i];
                    arrayInput[i] = arrayInput[i + 1];
                    arrayInput[i + 1] = temp;
                    swaps++;
                }
            }
            stillSwaps = (swaps != 0);
        }
    }

    private static void bubbleSort(int[] arrayInput) {
        bubbleSort(arrayInput, true);
    }

    private static void bubbleSort(int[] arrayInput, boolean ascending) {
        int length = arrayInput.length;
        int temp;
        boolean isSorted;

        for (int i = 0; i < length; i++){
            isSorted = true;
            for (int j = 1; j < (length - i); j ++){

                if (ascending){
                    if (arrayInput[j - 1] > arrayInput[j]) {
                        temp = arrayInput[j - 1];
                        arrayInput[j - 1] = arrayInput[j];
                        arrayInput[j] = temp;
                        isSorted = false;
                    }
                } else {
                    if (arrayInput[j - 1] < arrayInput[j]) {
                        temp = arrayInput[j - 1];
                        arrayInput[j - 1] = arrayInput[j];
                        arrayInput[j] = temp;
                        isSorted = false;
                    }
                }
            }
            if (isSorted) break;
        }

    }


}
