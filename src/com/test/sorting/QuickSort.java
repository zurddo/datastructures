package com.test.sorting;

import java.util.Arrays;

public class QuickSort {

    public static void main(String[] args) {

        int[] input = {10, 8, 99, 7, 1, 5, 88, 9};

        System.out.println("Before sorting: " + Arrays.toString(input));

        quickSort(input, 0, input.length - 1);

        System.out.println("After sorting: " + Arrays.toString(input));
    }


    private static void quickSort(int[] input, int start, int end) {
        if(start < end){
            // partitioning index element is now at right place
            int partitionIndex = partition(input, start, end);

            quickSort(input, start, partitionIndex - 1);
            quickSort(input, partitionIndex + 1, end);
        }
    }

    /** This function takes last element as pivot, places the pivot element at its correct
     position in sorted array, and places all smaller (smaller than pivot) to left of
     pivot and all greater elements to right of pivot
     */
    private static int partition(int[] input, int start, int end) {

        int pivot = input[end];
        int i = start - 1; // index of smaller element

        // Go through all the array
        for (int j = start; j < end; j++) {
            if (input[j] < pivot) {
                i++;

                // swap position
                int temp = input[i];
                input[i] = input[j];
                input[j] = temp;
            }
        }

        // swap input[i + 1] and input[end] (the pivot)
        int temp = input[i +1];
        input[i + 1] = input[end];
        input[end] = temp;

        return i + 1;
    }


}
