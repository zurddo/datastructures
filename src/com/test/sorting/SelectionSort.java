package com.test.sorting;

import java.util.Arrays;

public class SelectionSort {

    public static void main(String[] args) {

        int[] input = {10, 8, 99, 7, 1, 5, 88, 9};
        int[] input2 = {1, 5, 7, 8, 9, 10, 88, 99};

        System.out.println("Before sorting: " + Arrays.toString(input));

        selectionSort(input);

        System.out.println("After sorting: " + Arrays.toString(input));

        selectionSortDescending(input);

        System.out.println("After sorting descending: " + Arrays.toString(input));

    }

    private static void selectionSort(int[] input) {
        int length = input.length;
        int smallestIndex;
        int temp;
        for (int i = 0; i < length; i++){
            smallestIndex = i;
            for (int j = i + 1; j < length; j++) {
                if(input[j] < input[smallestIndex]){
                    smallestIndex = j;
                }
            }
            if(smallestIndex != i) {
                temp = input[i];
                input[i] = input[smallestIndex];
                input[smallestIndex] = temp;
            }
        }
    }

    private static void selectionSortDescending(int[] input) {
        int length = input.length;
        int smallestIndex;
        int temp;
        for (int i = 0; i < length; i++){
            smallestIndex = i;
            for (int j = i + 1; j < length; j++) {
                if(input[j] > input[smallestIndex]){
                    smallestIndex = j;
                }
            }
            if(smallestIndex != i) {
                temp = input[i];
                input[i] = input[smallestIndex];
                input[smallestIndex] = temp;
            }
        }
    }
}
