package com.test.stackarray;

public class StackArray {

  // Initialize array that represents the items in the stack
  // initialize in 0 so avoid null checks-
  private int[] items = new int[0];

  // Number of elements in the stack (not the length of the stack)
  private int size;

  // push method
  public void push(int item){

    // check if there is space in current array
    if(size == items.length){
      // create/resize array
      int newLength = size == 0 ? 10 : size * 2;

      // allocate and copy
      int[] newArray = new int[newLength];
      System.arraycopy(items, 0, newArray, 0, items.length);
      items = newArray;

    }

    items[size] = item;
    size++;

  }

  public int getSize() {
    return size;
  }
}
