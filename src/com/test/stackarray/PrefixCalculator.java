package com.test.stackarray;

import java.util.Stack;

public class PrefixCalculator {

    public static void main(String[] args){

        String toCalcule = "+9*28";
        Stack<Integer> stack = new Stack<Integer>();
        char[] charArray = toCalcule.toCharArray();
        for(int i = charArray.length - 1; i >= 0; i--){
            if(Character.isDigit(charArray[i])){
                stack.push(Character.getNumericValue(charArray[i]));
            } else {
                Integer rightSide = stack.pop();
                Integer leftSide = stack.pop();
                switch (charArray[i]) {
                    case '+':
                        stack.push(leftSide + rightSide);
                        break;
                    case '-':
                        stack.push(leftSide - rightSide);
                        break;
                    case '*':
                        stack.push(leftSide * rightSide);
                        break;
                    case '/':
                        stack.push(leftSide / rightSide);
                        break;
                    default:
                        throw new IllegalArgumentException("Not valid operator");
                }
            }
        }

        System.out.println(stack.pop());
    }
}
