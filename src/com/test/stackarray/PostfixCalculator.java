package com.test.stackarray;

import java.util.PriorityQueue;
import java.util.Stack;

public class PostfixCalculator {

  // java PostfixCalculator 567*+1-
  public static void main(String[] args){

    // assuming that values are passed in the arguments as a string
    // and it only digits 0-9
    // Create a stack
    Stack<Integer> stack = new Stack<Integer>();
    String operationString = args[0];
    for(char c : operationString.toCharArray()){
      if(Character.isDigit(c)){
        stack.push(Character.getNumericValue(c));
      } else {
        Integer rightSide = stack.pop();
        Integer leftSide = stack.pop();
        switch (c) {
          case '+':
            stack.push(leftSide + rightSide);
            break;
          case '-':
            stack.push(leftSide - rightSide);
            break;
          case '*':
            stack.push(leftSide * rightSide);
            break;
          case '/':
            stack.push(leftSide / rightSide);
            break;
          default:
            throw new IllegalArgumentException("Not valid operator");
        }
      }
    }
    System.out.println("Result is: " + stack.pop());
  }
}
