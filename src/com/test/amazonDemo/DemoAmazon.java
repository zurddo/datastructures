package com.test.amazonDemo;

import java.util.ArrayList;
import java.util.List;

public class DemoAmazon {

  public static void main(String[] args){
    int[] states = {1, 0, 0, 0, 0, 1, 0, 0};
    System.out.println(states.length);
    List<Integer> result = cellCompete(states, 1);
    System.out.println(result);
  }
  public static List<Integer> cellCompete(int[] states, int days)
  {
    List<Integer> changeList = new ArrayList<>();
    List<Integer> resultList = new ArrayList<>();
    // Throw exception if states < 8 elements
    if(states.length != 8)
      throw new RuntimeException("There are no 8 houses");
    // Iteration of number of days
    for(int i = 0; i < days; i++){
      // Iteration of elements in the array of states
      for(int j = 0; j < states.length; j++){
        // Case first element
        if(j == 0){
          changeList.add(checkValue(0, states[j], states[j+1]));
        } else if(j == 7){  // Case last element 100
          changeList.add(checkValue(states[j-1], states[j], 0));
        } else {
          changeList.add(checkValue(states[j - 1], states[j], states[j + 1]));
        }
      }
      resultList.addAll(changeList);
      changeList.clear();
    }
    return resultList;
    // WRITE YOUR CODE HERE
  }

  private static int checkValue(int before, int current, int next) {
    if(before == 0 && current == 0 && next == 1){
      return 1;
    }
    if(before == 1 && current == 0 && next == 0){
      return 1;
    }
    return 0;

  }
}
