package com.test.amazonDemo;

public class CalculateLcm {

  public static void main(String[] args){
    // Of two numbers.
    //System.out.println(lcmTwoNumbers(12,18));

    // of an array
    int[] myArray = {25, 50, 125, 625};
    System.out.println(findLCM(myArray, myArray.length));
    System.out.println(findLCM2(myArray));

  }

  // Find LCM in array
  static int findLCM(int arr[], int n)
  {
    int result = arr[0];
    for (int i = 1; i < n; i++){
      result = lcmTwoNumbers(arr[i], result);
      if(result == 1) {
        return 1;
      }
    }
    return result;
  }

  static int findLCM2(int[] myArray) {
    int min, max, x, lcm = 0;
    for(int i = 0; i<myArray.length; i++) {
      for(int j = i+1; j<myArray.length-1; j++) {
        if(myArray[i] > myArray[j]) {
          min = myArray[j];
          max = myArray[i];
        } else {
          min = myArray[i];
          max = myArray[j];
        }
        for(int k =0; k<myArray.length; k++) {
          x = k * max;
          if(x % min == 0) {
            lcm = x ;
          }
        }
      }
    }
    return lcm;
  }

  /*
  // adding extra validation
  public static int gcd(int number1, int number2) {
    if (number1 == 0 || number2 == 0) {
      return number1 + number2;
    } else {
      int absNumber1 = Math.abs(number1);
      int absNumber2 = Math.abs(number2);
      int biggerValue = Math.max(absNumber1, absNumber2);
      int smallerValue = Math.min(absNumber1, absNumber2);
      return gcd(biggerValue % smallerValue, smallerValue);
    }
  }*/

  // Using Euclid's algorithm
  static int gcd(int a, int b) {
    if (b == 0)
      return a;
    return gcd(b, a % b);
  }

  // Calculate the Least Common Multiple (LCM) using Euclidean Algorithm
  // the absolute value of the product of two numbers is equal to the product of their GCD and LCM.
  // gcd(a, b) * lcm(a, b) = |a * b|.
  // Then  lcm(a, b) = |a * b| / gcd(a, b).
  public static int lcmTwoNumbers(int number1, int number2) {
    if (number1 == 0 || number2 == 0)
      return 0;
    else {
      int gcd = gcd(number1, number2);
      return Math.abs(number1 * number2) / gcd;
    }
  }


}
