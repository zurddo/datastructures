package com.test.amazonDemo;

public class CalculateGcd {

  public static void main(String[] args)
  {
    int arr[] = { 2, 4, 6, 8, 16 };
    int n = arr.length;
    System.out.println(findGCD(arr, n));
  }

  // Function to find gcd of array of numbers
  static int findGCD(int arr[], int n)
  {
    int result = arr[0];
    for (int i = 1; i < n; i++){
      result = gcd(arr[i], result);
      if(result == 1) {
        return 1;
      }
    }
    return result;
  }

  // Using Euclid's algorithm
  static int gcd(int a, int b) {
    if (b == 0)
      return a;
    return gcd(b, a % b);
  }


  static int gcd2(int a, int b) {
    if (a == 0)
      return b;
    return gcd2(b % a, a);
  }


}
