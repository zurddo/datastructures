package com.test.dentalmonitoring;

import java.util.ArrayList;
import java.util.List;

/**
 * Écrire une classe Tree qui représente un arbre dont on veut pouvoir obtenir la racine via une méthode findRoot().
 *
 * Nous vous fournissons un prototype de classe Tree dont vous devez implémenter :
 *
 * La méthode addNode(int child, int parent) permettant d'initialiser l'arbre, noeud par noeud.
 * La méthode findRoot() qui renvoie la racine de l'arbre.
 *
 * Correspond à l'arbre :
 *
 *      5
 *      |
 *      4
 *     / \
 *    3   2
 *    |
 *    1
 */

public class TreeExample {

  private static Node root;

  public static void main(String[] args){
    TreeExample tree = new TreeExample();

    tree.addNode(1, 3);
    tree.addNode(3, 4);
    tree.addNode(4, 5);
    tree.addNode(2, 4);

    System.out.println(findRoot());

  }

  // TODO fix addNode with parameters and Add find method
  public static void addNode(int child, int parent) {

    if (root == null) {
      Node parentNode = new Node(parent);
      Node childNode = new Node(child);
      root = parentNode;
      root.getChildren().add(childNode);
    } else
    // Case where child needs to update the parent
    if (child == root.value) {
      Node newParent = new Node(parent);
      newParent.getChildren().add(root);
      root = newParent;
    } else {

      // search through elements of the tree for the parent
      // find parent to insert
      Node childNode = new Node(child);
    Node parentToInsert = findElement(root, childNode);
    System.out.println(parentToInsert.value);

    parentToInsert.getChildren().add(childNode);
    //Find parent to insertTo
   /* Node parentNode = new Node(parent);
    if(root.getChildren().contains(parentNode)){
      int find = root.getChildren().indexOf(parentNode);
      Node findNode = root.getChildren().get(find);
      Node childNode = new Node(child);
      findNode.getChildren().add(childNode);
    }*/
    }
    // What happen otherwise ?
  }

  public static int findRoot() {
    return root.getValue();
  }

  private static Node findElement(Node current, Node nodeValue){
    if(current == null) {
      return null;
    }
    if(current.getChildren().contains(nodeValue)){
      int index = current.getChildren().indexOf(nodeValue);
      return current.getChildren().get(index);
    }else {
      for (Node node : current.getChildren()) {
        return findElement(node, nodeValue);
      }
    }
    return null;
  }

  static class Node {
    private int value;
    private List<Node> children;

    public Node(int value) {
      this.value = value;
      children = new ArrayList<>();
    }

    public int getValue() {
      return value;
    }

    public void setValue(int value) {
      this.value = value;
    }

    public List<Node> getChildren() {
      return children;
    }

    public void setChildren(List<Node> children) {
      this.children = children;
    }
  }
}
