package com.test.dentalmonitoring;

import java.util.Stack;

public class DentalTest {

  public static void main(String[] args) {
    System.out.println(formatIsCorrect("((()))"));
    System.out.println(formatIsCorrect(null));
  }

  static boolean formatIsCorrect(String formatted ) {
    final Stack<Character> stack = new Stack<Character>();
    if(formatted == null || formatted.isEmpty()){
      return false;
    }

    for(char c: formatted.toCharArray()){
      if(c == '('){
        stack.push(c);
      }

      if(c == ')'){
        if(stack.isEmpty()){
          return false;
        }
        char last = stack.peek();
        if(last == '('){
          stack.pop();
        }else {
          return false;
        }
      }
    }
    return stack.isEmpty();
  }

  public static String checkBalancedParentesis(String expr)
  {
    if (expr.isEmpty())
      return "Balanced";

    Stack<Character> stack = new Stack<Character>();
    for (int i = 0; i < expr.length(); i++)
    {
      char current = expr.charAt(i);
      if (current == '{' || current == '(' || current == '[')
      {
        stack.push(current);
      }
      if (current == '}' || current == ')' || current == ']')
      {
        if (stack.isEmpty())
          return "Not Balanced";
        char last = stack.peek();
        if (current == '}' && last == '{' || current == ')' && last == '(' || current == ']' && last == '[')
          stack.pop();
        else
          return "Not Balanced";
      }
    }
    return stack.isEmpty()?"Balanced":"Not Balanced";
  }
}
