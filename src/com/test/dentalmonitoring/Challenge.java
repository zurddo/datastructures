package com.test.dentalmonitoring;

import java.util.*;

public class Challenge {

  public static void main(String[] args){

    // TEST1
    Integer[] jobs = {3, 10, 20, 1, 2};
    System.out.println(sjf(jobs, 0));

    // TEST2
    Integer[] jobs2 = {3, 10, 10, 20, 1, 2};
    System.out.println(sjf(jobs2, 2));
  }

  static Integer sjf(Integer[] jobs, Integer index) {

    List<Element> elementList = new ArrayList<>();
    // Burst of CC
    Integer burst = 0;

    // Feed the map with values and the index of original array
    for(int i = 0; i < jobs.length; i++) {
      elementList.add(new Element(jobs[i], i));
    }

    Collections.sort(elementList);
    for (Element element: elementList){
      if(index.equals(element.index)){
        burst += element.value;
        break;
      }
      burst += element.value;
    }

    return burst;
  }

  static class Element implements Comparable<Element> {
    private int value;
    private int index;

    public Element(int value, int index) {
      this.value = value;
      this.index = index;
    }

    public int compareTo(Element e) {
      return this.value - e.value;
    }
  }
}
