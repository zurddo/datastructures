package com.test.amazon;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AmazonAssessment {

  public static void main(String[] args){
    List<String> possibleFeatures = new ArrayList<String>(){{
      add("cetracular");
      add("deltacellular");
      add("anacell");
      add("betacellular");
      add("eurocell");
    }};

    List<String> requestFeatures = new ArrayList<String>(){{
      add("Best services provided by anacell anacell");
      add("deltacellular services than all other");
      add("betacellular has great services");
      add("anacell provides betacellular much better services than all other");
      add("provides betacellular much better services than all other");
      add("cetracular much better services than all other");
    }};

    System.out.println(popularNFeatures2(5,7, possibleFeatures, 6, requestFeatures));
  }

  private static ArrayList<String> popularNFeatures2(int numFeatures,
                                                     int topFeatures,
                                                     List<String> possibleFeatures,
                                                     int numFeaturesRequests,
                                                     List<String> requestFeatures) {
    if(topFeatures > numFeatures){
      return findOccurrences(possibleFeatures, requestFeatures)
          .collect(Collectors.toCollection(ArrayList::new));
    } else {
      return findOccurrences(possibleFeatures, requestFeatures)
          .limit(topFeatures)
          .collect(Collectors.toCollection(ArrayList::new));
    }

  }

  private static Stream<String> findOccurrences(List<String> possibleFeatures,
                                                List<String> requestFeatures){
    // Maybe a check for the length and be sure to be less expensive.
    return requestFeatures.stream()
        .flatMap(request -> possibleFeatures.stream()
            .filter(request::contains)
        ).collect(Collectors.groupingBy(feature -> feature, Collectors.counting()))
        .entrySet()
        .stream()
        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
        .sorted(Map.Entry.comparingByKey())
        .map(Map.Entry::getKey);
  }


  // Answer sent
  public static ArrayList<String> popularNFeatures(int numFeatures,
                                            int topFeatures,
                                            List<String> possibleFeatures,
                                            int numFeatureRequests,
                                            List<String> featureRequests) {

    // Create a map to hold feature and ocurrences
    Map<String, Integer> result = new HashMap<>();
    // Go through all the possible features
    int vote = 0;
    for(String feature: possibleFeatures){
      // Iterate through the request from the people
      for(String request: featureRequests){
        if(request.contains(feature)) {
          vote++;
          result.put(feature, vote);
        }
      }
      vote = 0;
    }
    List value2 = result.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
        .collect(Collectors.toList()).subList(0, topFeatures);
    System.out.println(value2);
    return new ArrayList<>(value2);
  }

}
