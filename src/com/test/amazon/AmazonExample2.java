package com.test.amazon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AmazonExample2 {

  private static int lastElement = 0;
  private static int oldLastElement = 0;
  private static List<Integer> resultArray = new ArrayList<>();

  public static void main(String[] args){

    // Have a variable that holds the last element of max distance


    // Given the array of scenes tagged -> tags should be "compressed" where the last
    // element of the same tag is found
    // To solve this should use hashing
    // Test 1
    //char[] scenesArray1 = new char[] {'a', 'b', 'c'}; // result should be [1,1,1]
    //int arrayLength = scenesArray1.length;

    /*while(lastElement < scenesArray1.length){
      resultArray.add(calculateScenes(scenesArray1, lastElement, arrayLength));
    }

    System.out.println(resultArray.toString());*/
    // Test 2
    char[] scenesArray2 = new char[] {'a', 'b', 'c', 'c', 'b', 'd', 'c', 'd'};

    int arrayLength = scenesArray2.length;
    while(lastElement < arrayLength) {
      resultArray.add(calculateScenes(scenesArray2, lastElement, arrayLength));
    }

    System.out.println(resultArray.toString());

  }

  private static int calculateScenes(char[] scenesArray1, int start, int length) {

    // Use Map to store element to first index mapping
    HashMap<Character, Integer> map = new HashMap<>();

    // Distance of the element
    int distance = 0;

    for(int i = start; i < length; i++) {
      if(!map.containsKey(scenesArray1[i])) {
        map.put(scenesArray1[i], i);
      } else {
        distance = Math.max(distance, i - map.get(scenesArray1[i]));
        lastElement = i;
      }
    }

    if(distance == 0) {
      lastElement++;
      return 1;
    }

    return distance;
  }
}
