package com;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Amazon {

    static public void main(String args[]){
        //char init[] = {'a', 'b', 'c', 'c', 'b', 'd', 'b', 'd'};
        //char init[] = {'a', 'b', 'c', 'c', 'b', 'd', 'c', 'd'};
        char init[] = {'a', 'c', 'c', 'b', 'd', 'c', 'd'};
        //char init[] = {'a', 'c', 'c', 'b', 'b', 'd', 'd'};
        //char init[] = {'a', 'b', 'c', 'b', 'c', 'b', 'c', 'b', 'c', 'd', 'c'};

        Map<Character, List<Integer>> coordinates = new HashMap<>();

        Integer finalArray[] = new Integer[init.length];

        for (int index = 0; index< init.length; index++) {
            if (coordinates.containsKey(init[index])){
                coordinates.get(init[index]).add(index);
            } else {
                List<Integer> tempList = new ArrayList<>();
                tempList.add(index);
                coordinates.put(init[index], tempList);
            }
        }

        int indexFA = 0;
        for (int i = 0; i< init.length;){
            List<Integer> list = coordinates.get(init[i]);
            Integer initialValue = list.get(0);
            if (initialValue < i){
                for (int j = 1; j < list.size(); j++){
                    if (list.get(j) >= i ){
                        initialValue = list.get(j);
                        break;
                    }
                }
            }
            finalArray[indexFA++] = Math.max(list.get(list.size() - 1) - initialValue, 1);
            if (list.get(list.size() - 1) + 1 <= init.length - 1) {
                i = list.get(list.size() - 1) + 1;
            } else {
                break;
            }
        }

        for (Integer i: finalArray) {
            System.out.println(i + ", ");
        }
    }
}
