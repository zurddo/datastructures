package com.test.round2;

import java.util.Scanner;

public class Staircase {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main (String[] args){
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        staircase(n);

        scanner.close();
    }

    public static void staircase(int size) {
        String str="#";
        for (int i = 0; i < size; i++)
        {
            System.out.printf("%" + size + "s%n", str); // "hidden" loop in the print.
            str = str + "#";
        }
    }

}
