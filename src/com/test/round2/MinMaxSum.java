package com.test.round2;

import java.util.Arrays;
import java.util.Scanner;

public class MinMaxSum {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] arr = new int[5];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < 5; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        miniMaxSum(arr);
        minMaxSum(arr);
        scanner.close();
    }

    // Complexity O(n log n)
    static void miniMaxSum(int[] arr) {
        Arrays.sort(arr);
        long sum = 0;
        int length = arr.length;
        for(int i = 0; i < length; i++){
            sum += arr[i];
        }
        long min = sum - arr[length - 1];
        long max = sum - arr[0];
        System.out.println(min + " " + max);
    }

    // Complexity O(n)
    static void minMaxSum(int[] arr) {
        int min = arr[0];
        int max = arr[0];
        long total = 0;
        for(int i = 0; i < arr.length; i++){
            total += arr[i];
            if(min > arr[i]){
                min = arr[i];
            }
            if(max < arr[i]){
                max = arr[i];
            }
        }
        System.out.println((total - max) + " " + (total - min));

    }
}
