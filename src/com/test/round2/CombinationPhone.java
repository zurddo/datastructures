package com.test.round2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number
 * could represent.
 *
 * A mapping of digit to letters (just like on the telephone buttons) is given below. Note that 1 does not
 * map to any letters.
 *
 * Example:
 *
 * Input: "23"
 * Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
 *
 *   The order of the numbers matters, can't be "da", "db", "cd"
 *   If there is only two digits then the combination would be two letters only.
 *   If there are three digits the combination of letters would be three.
 *
 * Input: "786"
 * Output: ["ptm", "ptn", "pto",
 *    "pum", "pun", "puo",
 *    "pvm", "pvn", "pvo",
 *    "qtm", "qtn", "qto",
 *    "qum", "qun", "quo",
 *    "qvm", "qvn", "qvo",
 *    "rtm", "rtn, "rto",
 *    "rum", "run", "ruo",
 *    "rvm", "rvn", "rvo",
 *    "stm", "stn", "sto",
 *    "sum", "sun", "suo",
 *    "svm", "svn", "svo"]
 *
 * 1. naive solution, iterate through each letter by numbers and have the combinations out of it.
 *     But it grows in complexity as it grows the number of digits at the input.
 *
 * 2. Have a dictionary of numbers with lettres associated
 * 	2 - abc
 * 	3 - def
 * 	4 - ghi
 *
 * Create a hashmap with key-values of the numbers.
 *
 * Iterate each digit of string and get all the combinations - How?
 *
 * Tip: Instead of for loop, do substrings to the strings, then iterate over each letter of the number
 * adding it to a result string.
 *
 * Time complexity : O(3^N x 4^M) where N is the number of digits in the input that maps
 * to 3 letters (e.g. 2, 3, 4, 5, 6, 8) and M is the number of digits in the input that maps
 * to 4 letters (e.g. 7, 9), and N+M is the total number digits in the input.
 *
 *
 * Space complexity: O(3^N x 4^M) has to keep all of them
 *
 */
public class CombinationPhone {

    static Map<String, String> phone = new HashMap<String, String>() {{
        put("2", "abc");
        put("3", "def");
        put("4", "ghi");
        put("5", "jkl");
        put("6", "mno");
        put("7", "pqrs");
        put("8", "tuv");
        put("9", "wxyz");
    }};

    static List<String> result = new ArrayList<String>();

    public static void main(String[] args) {

        String test = "786";

        letterCombinations(test);

        result.forEach(System.out::println);
    }

    public static List<String> letterCombinations(String digits) {
        if (digits.length() != 0)
            backtrack("", digits);
        return result;
    }

    public static void backtrack(String combination, String nextDigits){
        // there is no more digits to check
        if(nextDigits.length() == 0){
            result.add(combination);
        } else {
            // Iterate over all the letter of a digit
            String digit = nextDigits.substring(0, 1); // Take first digit
            String letters = phone.get(digit); // Get letter of digit
            for(char letter : letters.toCharArray()) {
                backtrack(combination + letter, nextDigits.substring(1)); // leave the first digit and continue with next one
            }
        }

    }

}
