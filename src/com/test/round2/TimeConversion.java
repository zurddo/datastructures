package com.test.round2;

import java.io.IOException;
import java.util.Scanner;

public class TimeConversion {

    /*
     * Complete the timeConversion function below.
     */
    static String timeConversion(String s) {
        /*
         * Write your code here.
         */
        String result = "";
        String[] splited = s.split(":");
        int hour = Integer.parseInt(splited[0]);
        if(s.contains("PM") && hour != 12){
            hour = hour + 12;
            result = String.valueOf(hour);
        } else if (s.contains("AM") && hour == 12){
            result = "00";
        } else {
            result = splited[0];
        }

        return result + ":" + splited[1] + ":" + splited[2].substring(0,2);
    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {


        String s = scan.nextLine();

        String result = timeConversion(s);

        System.out.println(result);
    }
}
