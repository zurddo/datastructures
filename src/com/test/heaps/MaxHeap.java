package com.test.heaps;

import java.util.Arrays;

public class MaxHeap {

    int capacity = 10;
    int size = 0;

    int[] items = new int[capacity];

    // Util methods for the tree
    // Get index of left, right or parent nodes
    private int getLeftChildIndex(int parentIndex) { return 2 * parentIndex + 1; }
    private int getRightChildIndex(int parentIndex) { return 2 * parentIndex + 2; }
    private int getParentIndex(int childIndex) { return (childIndex - 1) / 2; }

    // Check if has left, right or parent
    private boolean hasLeftChild(int index) { return getLeftChildIndex(index) < size; }
    private boolean hasRightChild(int index) { return getRightChildIndex(index) < size; }
    private boolean hasParent(int index) { return getParentIndex(index) >= 0; }

    // Get the left, right or parent node
    private int leftChild(int index) { return items[getLeftChildIndex(index)]; }
    private int rightChild(int index) { return items[getRightChildIndex(index)]; }
    private int parent(int index) { return items[getParentIndex(index)]; }

    // Swap method
    private void swap(int indexOne, int indexTwo) {
        int temp = items[indexOne];
        items[indexOne] = items[indexTwo];
        items[indexTwo] = temp;
    }

    // Ensure capacity of the array
    private void ensureCapacity(){
        if(size == capacity){
            items = Arrays.copyOf(items, capacity * 2); // copies and doubles the array
            capacity *= 2;
        }
    }

    // Implements peek
    private int peek(){
        if (size == 0) throw new IllegalStateException();
        // Get the first element of the array
        return items[0];
    }

    // Removes element of the array (poll or remove)
    private int poll(){
        if (size == 0) throw new IllegalStateException();
        int item = items[0];
        items[0] = items[size - 1]; // Pass the last element to the first element
        size--;
        heapifyDown();
        return item;
    }

    public void heapifyDown(){
        int index = 0; // Start at the first index
        while(hasLeftChild(index)){ // check only left child, if there is no left there is no right child
            // get the small child index starting for the left
            int smallerChildIndex = getLeftChildIndex(index);
            if (hasRightChild(index) && rightChild(index) < leftChild(index)){
                smallerChildIndex = getRightChildIndex(index);
            }
            if( items[index] < items[smallerChildIndex]) { // If the current value at index if smaller than smallest child then break
                swap(index, smallerChildIndex);
            } else { // if the value at smallerChild is less than current value then swap
               break;
            }
            index = smallerChildIndex;
        }
    }

    public void add(int value){
        ensureCapacity();
        items[size] = value;
        size++;
        heapifyUp();
    }

    public void heapifyUp() {
        int index = size - 1; // take index of last element
        while (hasParent(index) && parent(index) < items[index]) {
            swap(getParentIndex(index), index); // swap the indexes
            index = getParentIndex(index);
        }
    }

    public void printHeap() {
        for (int i = 0; i < size; i++) {
            System.out.print(items[i] + " ");
        }
    }
}
