package com.test.heaps;

public class BinaryHeapsTest {

    public static void main(String[] main){
        MaxHeap maxHeap = new MaxHeap();
//    maxHeap.add(12);
//    maxHeap.add(23);
//    maxHeap.add(2);
//    maxHeap.add(112);
//    maxHeap.add(112);
//    maxHeap.add(2);
//    maxHeap.add(53);
//    maxHeap.add(1);
//    maxHeap.add(9);
//    maxHeap.add(5);
//    maxHeap.add(8);
//    maxHeap.add(3);
//    maxHeap.add(6);

        maxHeap.add(5);
        maxHeap.add(3);
        maxHeap.add(17);
        maxHeap.add(10);
        maxHeap.add(84);
        maxHeap.add(19);
        maxHeap.add(6);
        maxHeap.add(22);
        maxHeap.add(9);
        System.out.println("Print max heap: ");
        maxHeap.printHeap();

        MinHeap minHeap = new MinHeap();
        minHeap.add(5);
        minHeap.add(3);
        minHeap.add(17);
        minHeap.add(10);
        minHeap.add(84);
        minHeap.add(19);
        minHeap.add(6);
        minHeap.add(22);
        minHeap.add(9);

        System.out.println("\nPrint min heap: ");
        minHeap.printHeap();
    }
}
