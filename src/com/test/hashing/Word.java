package com.test.hashing;

public class Word {
    String name;

    public Word(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     *
     * Override equals to get the same value with the same instance as key.
     * Use with hashCode() as well when get() from hashtable class.
     *
     * We assume 'name' is non-null otherwise check nullability
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Word word = (Word) o;
        return getName().equals(word.getName());
    }

    /**
     * Get the hashcode of 'name' so it will be always the same.
     */
    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
